ASM_SRC:=$(wildcard bootloader_src/*asm)
ASM_OBJ:=$(patsubst bootloader_src/%.asm,build/%.o,$(ASM_SRC))

RUST_LIB:=target/x86_64-asteria/debug/libasteria.a
RUST_SRC:=$(shell find src userland -type f -name '*.rs')

.PHONY: default run debug clean

default: boot.img

run: boot.img
	qemu-system-x86_64 \
		-bios /usr/share/edk2-ovmf/x64/OVMF.fd \
		-drive file=boot.img,format=raw,media=disk \
		-serial stdio

debug: boot.img
	qemu-system-x86_64 \
		-s -S \
		-bios /usr/share/edk2-ovmf/x64/OVMF.fd \
		-drive file=boot.img,format=raw,media=disk \
		-d int,cpu_reset &
	gdb -ex "symbol-file efi/kernel.bin" -ex "target remote localhost:1234"

clean:
	rm -rf build

boot.img: build/kernel.bin efi/EFI/BOOT/limine.cfg
	cp build/kernel.bin efi/kernel.bin
	sudo ./buildfs.sh

build/kernel.bin: bootloader_src/linker.ld src/possum.ppm src/font.ppm $(ASM_OBJ) $(RUST_LIB)
	ld -n -T bootloader_src/linker.ld -o $@ $(ASM_OBJ) $(RUST_LIB)

build/%.o: bootloader_src/%.asm
	mkdir -p $(shell dirname $@)
	nasm -g -f elf64 $< -o $@

$(RUST_LIB): $(RUST_SRC)
	cargo build

src/possum.ppm: resources/possum.jpg
	convert $< $@

src/font.ppm: resources/Chroma48.png
	convert $< $@
