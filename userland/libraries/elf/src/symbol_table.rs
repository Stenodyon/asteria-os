use crate::strided::Strided;
use crate::{Elf, SectionHeader, SectionHeaderIndex, SectionHeaderIndexRaw};

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct SymbolTableEntry {
    pub st_name: u32,
    pub st_info: SymbolInfo,
    pub st_other: SymbolVisibility,
    pub st_shndx: SectionHeaderIndexRaw,
    pub st_value: u64,
    pub st_size: u64,
}

#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct SymbolInfo(u8);

impl SymbolInfo {
    pub fn symbol_type(&self) -> SymbolType {
        SymbolType::from(self.0 & 0xf)
    }

    pub fn binding(&self) -> SymbolBinding {
        SymbolBinding::from(self.0 >> 4)
    }
}

impl core::fmt::Debug for SymbolInfo {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "SymbolInfo({:?} {:?})",
            self.symbol_type(),
            self.binding()
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SymbolBinding {
    Local = 0,
    Global = 1,
    Weak = 2,
    LoOS = 10,
    HiOS = 12,
    LoProc = 13,
    HiProc = 15,

    Invalid,
}

impl From<u8> for SymbolBinding {
    fn from(value: u8) -> Self {
        match value {
            0 => Self::Local,
            1 => Self::Global,
            2 => Self::Weak,
            10 => Self::LoOS,
            12 => Self::HiOS,
            13 => Self::LoProc,
            15 => Self::HiProc,
            _ => Self::Invalid,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SymbolType {
    NoType = 0,
    Object = 1,
    Func = 2,
    Section = 3,
    File = 4,
    Common = 5,
    TLS = 6,
    LoOS = 10,
    HiOS = 12,
    LoProc = 13,
    HiProc = 15,

    Invalid,
}

impl From<u8> for SymbolType {
    fn from(value: u8) -> Self {
        match value {
            0 => Self::NoType,
            1 => Self::Object,
            2 => Self::Func,
            3 => Self::Section,
            4 => Self::File,
            5 => Self::Common,
            6 => Self::TLS,
            10 => Self::LoOS,
            12 => Self::HiOS,
            13 => Self::LoProc,
            15 => Self::HiProc,
            _ => Self::Invalid,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
#[non_exhaustive]
pub enum SymbolVisibility {
    Default = 0,
    Internal = 1,
    Hidden = 2,
    Protected = 3,
}

#[derive(Clone, Copy)]
pub struct SymbolTable<'a> {
    elf: &'a Elf<'a>,
    header: &'a SectionHeader,
    entries: Strided<'a, SymbolTableEntry>,
}

impl<'a> SymbolTable<'a> {
    pub fn new(elf: &'a Elf<'a>, header: &'a SectionHeader) -> Self {
        let start = header.sh_offset as usize;
        let end = start + header.sh_size as usize;
        let data = &elf.bytes[start..end];
        let count = header.sh_size.div_euclid(header.sh_entsize) as usize;

        let entries = unsafe {
            Strided::<SymbolTableEntry>::new(data, count, header.sh_entsize as usize).unwrap()
        };

        Self {
            elf,
            header,
            entries,
        }
    }

    pub fn len(&self) -> usize {
        self.entries.len()
    }

    pub fn get(&self, symbol_index: usize) -> Symbol<'a> {
        let entry = &self.entries[symbol_index];

        let name = if entry.st_name != 0 {
            self.elf
                .get_string_from(entry.st_name as usize, self.header.sh_link.into())
        } else {
            None
        };

        Symbol {
            name,
            info: entry.st_info,
            visibility: entry.st_other,
            section_header_index: entry.st_shndx.into(),
        }
    }
}

impl core::fmt::Debug for SymbolTable<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("SymbolTable")
            .field("entries", &self.entries)
            .finish()
    }
}

impl<'a> IntoIterator for SymbolTable<'a> {
    type Item = Symbol<'a>;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        Iter {
            symbol_table: self,
            current: 0,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Symbol<'a> {
    pub name: Option<&'a str>,
    pub info: SymbolInfo,
    pub visibility: SymbolVisibility,
    pub section_header_index: SectionHeaderIndex,
}

#[derive(Debug, Clone, Copy)]
pub struct Iter<'a> {
    symbol_table: SymbolTable<'a>,
    current: usize,
}

impl<'a> Iterator for Iter<'a> {
    type Item = Symbol<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current >= self.symbol_table.len() {
            return None;
        }

        let symbol = self.symbol_table.get(self.current);
        self.current += 1;
        Some(symbol)
    }
}
