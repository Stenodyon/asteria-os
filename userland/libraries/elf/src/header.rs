use crate::SectionHeaderIndexRaw;

/// ELF file header.
#[derive(Debug)]
#[repr(C)]
pub struct Header {
    pub e_ident: Ident,
    pub e_type: ObjetFileType,
    pub e_machine: Machine,
    pub e_version: u32,

    /// Virtual address to which the system transfers control. If no entry point this is null.
    pub e_entry: u64,

    /// Program header table's offset in bytes into the file.
    pub e_phoff: u64,

    /// Section header table's offset in bytes into the file.
    pub e_shoff: u64,

    /// Processor-specific flags.
    pub e_flags: u32,

    /// ELF header size in bytes.
    pub e_ehsize: u16,

    /// Size in bytes of a program header.
    pub e_phentsize: u16,

    /// Number of entries in the program header table.
    pub e_phnum: u16,

    /// Size in bytes of a section header.
    pub e_shentsize: u16,

    /// Number of entries in the section header table.
    pub e_shnum: u16,

    /// Index in the section header table of the section header that contains the section name
    /// string table.
    pub e_shstrndx: SectionHeaderIndexRaw,
}

#[derive(Clone, Copy)]
#[repr(C, packed)]
pub struct Ident {
    /// Must be `[0x7f, b'E', b'L', b'F']`
    pub signature: Signature,
    pub class: Class,
    pub data_encoding: DataEncoding,

    /// Must be the same as the header's version.
    pub file_version: u8,
    pub os_abi: OsAbi,
    pub abi_version: u8,
    padding: [u8; 16 - 9],
}

impl core::fmt::Debug for Ident {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Ident")
            .field("signature", &self.signature)
            .field("class", &self.class)
            .field("data_encoding", &self.data_encoding)
            .field("file_version", &self.file_version)
            .field("os_abi", &self.os_abi)
            .field("abi_version", &self.abi_version)
            .finish()
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
pub struct Signature(pub [u8; 4]);

impl Signature {
    pub fn is_valid(&self) -> bool {
        self.0 == [0x7f, b'E', b'L', b'F']
    }
}

impl core::fmt::Debug for Signature {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        if self.is_valid() {
            write!(f, "Valid(0x7f ELF)")
        } else {
            write!(f, "Invalid({:?})", self.0)
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u8)]
#[non_exhaustive]
pub enum Class {
    /// Invalid class.
    None = 0,
    Class32 = 1,
    Class64 = 2,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u8)]
#[non_exhaustive]
pub enum DataEncoding {
    /// Invalid data encoding.
    None = 0,
    /// Little-endian
    Data2LSB = 1,
    /// Big-endian
    Data2MSB = 2,
}

/// Identifies OS- or ABI-specific *extensions*.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u8)]
#[non_exhaustive]
pub enum OsAbi {
    // XXX: Incomplete
    /// No extensions or unspecified.
    None = 0,
    /// Hewlett-Packard HP-UX
    Hpux = 1,
    NetBSD = 2,
    /// GNU & Linux
    Gnu = 3,
    /// Sun Solaris
    Solaris = 6,
    AIX = 7,
    IRIX = 8,
    FreeBSD = 9,
    /// Compaq TRU64 UNIX
    Tru64 = 10,
    /// Novell Modesto
    Modesto = 11,
    OpenBSD = 12,
    OpenVMS = 13,
    /// Hewlett-Packard Non-Stop Kernel
    Nsk = 14,
    /// Amiga Research OS
    ArOs = 15,
    FenixOS = 16,
    /// Nuxi CloudABI
    CloudAbi = 17,
    /// Stratus Technologies OpenVOS
    OpenVOS = 18,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u16)]
#[non_exhaustive]
pub enum ObjetFileType {
    None = 0,
    Relocatable = 1,
    Executable = 2,
    Shared = 3,
    Core = 4,

    LoOs = 0xfe00,
    HiOs = 0xfeff,
    LoProc = 0xff00,
    HiProc = 0xffff,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u16)]
#[non_exhaustive]
pub enum Machine {
    // XXX: Incomplete
    None = 0,

    X86_64 = 62,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u32)]
#[non_exhaustive]
pub enum Version {
    None = 0,
    Current = 1,
}
