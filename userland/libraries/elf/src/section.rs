use crate::strided::Strided;
use crate::{Elf, SymbolTable};

#[derive(Debug)]
#[repr(C)]
pub struct SectionHeader {
    /// Byte index into the section header string table.
    pub sh_name: u32,
    /// Categorizes the section's contents and semantics.
    pub sh_type: SectionType,
    /// 1-bit flags for misc. attributes.
    pub sh_flags: SectionFlags,
    /// If the section is to appear in the memory image of a process, this value gives the address
    /// at which the section's first byte should reside. Otherwise 0.
    pub sh_addr: u64,
    /// Byte offset into the file of the beginning of the section.
    pub sh_offset: u64,
    /// Size in bytes of the section.
    pub sh_size: u64,
    /// Section header table index link, whose interpretation depends on the section type.
    pub sh_link: SectionHeaderIndexRaw,
    pub sh_link_pad: u16,
    /// Extra information whose interpretation depends on the section type.
    pub sh_info: u32,
    /// Alignment constraint of the section in bytes if it has one. Otherwise 0.
    pub sh_addralign: u64,
    /// If the section holds a table of fixed-size entries, this is the size of one such entry in bytes. Otherwise 0.
    pub sh_entsize: u64,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u32)]
#[non_exhaustive]
pub enum SectionType {
    Null = 0,
    ProgramBits = 1,
    SymbolTable = 2,
    StringTable = 3,
    RelocationAddends = 4,
    SymbolHashTable = 5,
    DynamicLinking = 6,
    Note = 7,
    NoBits = 8,
    Relocation = 9,
    /// Reserved, semantics unspecified.
    SHLib = 10,
    DynamicSymbols = 11,
    InitArray = 14,
    FiniArray = 15,
    PreinitArray = 16,
    Group = 17,
    SymTabSHNDX = 18,
    LoOs = 0x60000000,
    HiOs = 0x6fffffff,
    LoProc = 0x70000000,
    HiProc = 0x7fffffff,
    LoUser = 0x80000000,
    HiUser = 0xffffffff,
}

#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct SectionFlags(u64);

impl SectionFlags {
    /// The section contains data that should be writable during process execution.
    pub fn write(&self) -> bool {
        (self.0 & 0x1) != 0
    }

    /// The section occupies memory during process execution.
    pub fn alloc(&self) -> bool {
        (self.0 & 0x2) != 0
    }

    /// The section contains executable machine instructions.
    pub fn execinstr(&self) -> bool {
        (self.0 & 0x4) != 0
    }

    /// The data in the section may be merged to eliminate duplication.
    pub fn merge(&self) -> bool {
        (self.0 & 0x10) != 0
    }

    /// The data elements in the section consists of null-terminated character strings.
    pub fn strings(&self) -> bool {
        (self.0 & 0x20) != 0
    }

    /// The `sh_info` field of this section holds a section header table index.
    pub fn info_link(&self) -> bool {
        (self.0 & 0x40) != 0
    }

    /// This flag adds special ordering requirements for link editors.
    pub fn link_order(&self) -> bool {
        (self.0 & 0x80) != 0
    }

    /// This section requires special OS-specific processing.
    pub fn os_nonconforming(&self) -> bool {
        (self.0 & 0x100) != 0
    }

    /// This section is a member of a section group.
    pub fn group(&self) -> bool {
        (self.0 & 0x200) != 0
    }

    /// This section holds *Thread-Local Storage*.
    pub fn tls(&self) -> bool {
        (self.0 & 0x400) != 0
    }

    /// The section contains compressed data.
    pub fn compressed(&self) -> bool {
        (self.0 & 0x800) != 0
    }

    // TODO: SHF_MASKOS & SHF_MASKPROC
}

impl core::fmt::Debug for SectionFlags {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", if self.compressed() { "C" } else { "-" })?;
        write!(f, "{}", if self.tls() { "T" } else { "-" })?;
        write!(f, "{}", if self.group() { "G" } else { "-" })?;
        write!(f, "{}", if self.os_nonconforming() { "N" } else { "-" })?;
        write!(f, "{}", if self.link_order() { "L" } else { "-" })?;
        write!(f, "{}", if self.info_link() { "I" } else { "-" })?;
        write!(f, "{}", if self.strings() { "S" } else { "-" })?;
        write!(f, "{}", if self.merge() { "M" } else { "-" })?;
        write!(f, "{}", if self.execinstr() { "X" } else { "-" })?;
        write!(f, "{}", if self.alloc() { "A" } else { "-" })?;
        write!(f, "{}", if self.write() { "W" } else { "-" })
    }
}

impl core::fmt::Display for SectionFlags {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let write = if self.write() { "WRITE" } else { "" };
        let alloc = if self.alloc() { " ALLOC" } else { "" };
        let execinstr = if self.execinstr() { " EXECINSTR" } else { "" };
        let merge = if self.merge() { " MERGE" } else { "" };
        let strings = if self.strings() { " STRINGS" } else { "" };
        let info_link = if self.info_link() { " INFO_LINK" } else { "" };
        let link_order = if self.link_order() { " LINK_ORDER" } else { "" };
        let os_nonconforming = if self.os_nonconforming() {
            " OS_NONCONFORMING"
        } else {
            ""
        };
        let group = if self.group() { " GROUP" } else { "" };
        let tls = if self.tls() { " TLS" } else { "" };
        let compressed = if self.compressed() { " COMPRESSED" } else { "" };

        write!(f, "{write}{alloc}{execinstr}{merge}{strings}{info_link}{link_order}{os_nonconforming}{group}{tls}{compressed}")
    }
}

#[derive(Clone, Copy)]
pub struct Sections<'a> {
    elf: &'a Elf<'a>,
}

impl<'a> Sections<'a> {
    pub(crate) fn new(elf: &'a Elf<'a>) -> Self {
        Self { elf }
    }

    pub fn get(&self, section_index: usize) -> Option<Section<'a>> {
        if section_index >= self.elf.section_headers.len() {
            return None;
        }

        let header = &self.elf.section_headers[section_index];

        if header.sh_type == SectionType::SymbolTable {
            let symbol_table = SymbolTable::new(self.elf, header);
            let section = Section::SymbolTable(symbol_table);
            return Some(section);
        }

        let name = self.elf.get_string(header.sh_name as usize);
        let addr = if header.sh_addr != 0 {
            Some(header.sh_addr)
        } else {
            None
        };
        let data = if header.sh_type != SectionType::NoBits {
            let start = header.sh_offset as usize;
            let end = start + header.sh_size as usize;
            &self.elf.bytes[start..end]
        } else {
            &[]
        };

        let link = match header.sh_type {
            SectionType::DynamicLinking
            | SectionType::SymbolHashTable
            | SectionType::Relocation
            | SectionType::RelocationAddends
            | SectionType::SymbolTable
            | SectionType::DynamicSymbols
            | SectionType::Group
            | SectionType::SymTabSHNDX => Some(header.sh_link),
            _ => None,
        };

        let info = match header.sh_type {
            SectionType::Relocation
            | SectionType::RelocationAddends
            | SectionType::SymbolTable
            | SectionType::DynamicSymbols
            | SectionType::Group => Some(header.sh_info as usize),
            _ => None,
        };

        let entry_size = if header.sh_entsize != 0 {
            Some(header.sh_entsize as usize)
        } else {
            None
        };

        let section = Section::Generic {
            name,
            section_type: header.sh_type,
            flags: header.sh_flags,
            addr,
            data,
            link,
            info,
            entry_size,
        };
        Some(section)
    }

    pub fn len(&self) -> usize {
        self.elf.section_headers.len()
    }
}

impl core::fmt::Debug for Sections<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Sections {{ .. }}")
    }
}

impl<'a> IntoIterator for Sections<'a> {
    type Item = Section<'a>;
    type IntoIter = Iter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        Iter {
            sections: self,
            current: 0,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Section<'a> {
    Generic {
        name: Option<&'a str>,
        section_type: SectionType,
        flags: SectionFlags,
        addr: Option<u64>,
        data: &'a [u8],
        link: Option<SectionHeaderIndexRaw>,
        info: Option<usize>,
        entry_size: Option<usize>,
    },

    SymbolTable(SymbolTable<'a>),
}

impl Section<'_> {
    pub fn name(&self) -> Option<&str> {
        match self {
            Section::Generic { name, .. } => *name,
            Section::SymbolTable(_) => Some(".symtab"),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
pub struct SectionHeaderIndexRaw(u16);

impl core::fmt::Debug for SectionHeaderIndexRaw {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        SectionHeaderIndex::from(*self).fmt(f)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SectionHeaderIndex {
    Undef,
    Index(usize),
    Proc(usize),
    Os(usize),
    Absolute,
    Common,
    XIndex,

    Garbage(usize),
}

impl From<SectionHeaderIndexRaw> for SectionHeaderIndex {
    fn from(value: SectionHeaderIndexRaw) -> Self {
        match value.0 {
            0 => Self::Undef,
            1..=0xfeff => Self::Index(value.0 as usize),
            0xff00..=0xff1f => Self::Proc(value.0 as usize),
            0xff20..=0xff3f => Self::Os(value.0 as usize),
            0xfff1 => Self::Absolute,
            0xfff2 => Self::Common,
            0xffff => Self::XIndex,

            other => Self::Garbage(other as usize),
        }
    }
}

impl<'a> core::ops::Index<SectionHeaderIndex> for Strided<'a, SectionHeader> {
    type Output = SectionHeader;

    fn index(&self, index: SectionHeaderIndex) -> &Self::Output {
        if let SectionHeaderIndex::Index(index) = index {
            &self[index]
        } else {
            panic!("Invalid section header index {index:?}");
        }
    }
}

impl<'a> core::ops::Index<SectionHeaderIndexRaw> for Strided<'a, SectionHeader> {
    type Output = SectionHeader;

    fn index(&self, index: SectionHeaderIndexRaw) -> &Self::Output {
        &self[SectionHeaderIndex::from(index)]
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Iter<'a> {
    sections: Sections<'a>,
    current: usize,
}

impl<'a> Iterator for Iter<'a> {
    type Item = Section<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current >= self.sections.len() {
            return None;
        }

        let section = self.sections.get(self.current);
        self.current += 1;
        section
    }
}
