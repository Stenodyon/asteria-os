use core::fmt::Debug;
use core::marker::PhantomData;
use core::mem::size_of;

pub struct Strided<'a, T> {
    base: *const T,
    count: usize,
    stride: usize,
    _phantom: PhantomData<&'a T>,
}

impl<'a, T> Strided<'a, T> {
    // TODO: Replace Option with Result?
    pub unsafe fn new(data: &'a [u8], count: usize, stride: usize) -> Option<Self> {
        let required_size = (count - 1) * stride + size_of::<T>();
        if data.len() < required_size {
            return None;
        }

        let this = Self {
            base: data.as_ptr() as *const T,
            count,
            stride,
            _phantom: PhantomData,
        };
        Some(this)
    }

    pub fn get(&self, index: usize) -> Option<&T> {
        if index > self.count {
            return None;
        }

        let value = unsafe { &*(self.base.byte_add(self.stride * index)) };
        Some(value)
    }

    pub fn len(&self) -> usize {
        self.count
    }
}

impl<T> Clone for Strided<'_, T> {
    fn clone(&self) -> Self {
        let Self {
            base,
            count,
            stride,
            ..
        } = self;

        Self {
            base: *base,
            count: *count,
            stride: *stride,
            _phantom: PhantomData,
        }
    }
}

impl<T> Copy for Strided<'_, T> {}

impl<T: Debug> Debug for Strided<'_, T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_list().entries(self.into_iter()).finish()
    }
}

impl<T> core::ops::Index<usize> for Strided<'_, T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        match self.get(index) {
            Some(value) => value,

            None => panic!(
                "Index out of bounds, index is {index}, bound is {}",
                self.count
            ),
        }
    }
}

impl<'a, T> IntoIterator for Strided<'a, T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        let Self {
            base,
            count,
            stride,
            _phantom,
        } = self;

        Iter {
            base,
            count,
            stride,
            _phantom,
        }
    }
}

pub struct Iter<'a, T> {
    base: *const T,
    count: usize,
    stride: usize,
    _phantom: PhantomData<&'a T>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count == 0 {
            return None;
        }

        let value = unsafe { &*self.base };
        self.base = unsafe { self.base.byte_add(self.stride) };
        self.count -= 1;

        Some(value)
    }
}
