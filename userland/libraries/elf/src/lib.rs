//! Provides structures and helpers to read and load ELF executables.

#![no_std]
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]

mod header;
pub mod section;
mod strided;
pub mod symbol_table;

use core::mem::size_of;

pub use header::*;
use section::*;
use strided::Strided;
use symbol_table::*;

pub struct Elf<'a> {
    pub(crate) bytes: &'a [u8],
    pub header: &'a Header,
    pub section_headers: Strided<'a, SectionHeader>,
}

impl<'a> Elf<'a> {
    pub fn from_bytes(bytes: &'a [u8]) -> Result<Self, Error> {
        if bytes.len() < size_of::<Header>() {
            return Err(Error::UnexpectedEOF);
        }

        let header: &'a Header = unsafe { core::mem::transmute(bytes.as_ptr()) };

        if !header.e_ident.signature.is_valid() {
            return Err(Error::InvalidSignature(header.e_ident.signature));
        }

        // 8-byte aligned
        let section_header_stride = (header.e_shentsize as usize).next_multiple_of(8);
        let section_headers = unsafe {
            Strided::<SectionHeader>::new(
                &bytes[header.e_shoff as usize..],
                header.e_shnum as usize,
                section_header_stride,
            )
        }
        .unwrap();

        if SectionHeaderIndex::from(header.e_shstrndx) != SectionHeaderIndex::Undef {
            let string_table_header = &section_headers[header.e_shstrndx];
            if string_table_header.sh_type != SectionType::StringTable {
                return Err(Error::InvalidStringTable);
            }
        }

        let elf = Self {
            bytes,
            header,
            section_headers,
        };
        Ok(elf)
    }

    pub fn sections<'elf>(&'elf self) -> Sections<'elf> {
        Sections::new(self)
    }

    fn get_string(&self, offset: usize) -> Option<&'a str> {
        let string_header_index = SectionHeaderIndex::from(self.header.e_shstrndx);
        if string_header_index == SectionHeaderIndex::Undef {
            return None;
        }

        self.get_string_from(offset, string_header_index)
    }

    fn get_string_from(&self, offset: usize, table_index: SectionHeaderIndex) -> Option<&'a str> {
        let string_section_header = &self.section_headers[table_index];

        if string_section_header.sh_type != SectionType::StringTable {
            panic!("Tried to read string from non-string table section");
        }

        let section_base = string_section_header.sh_offset as usize;
        let section_offset = string_section_header.sh_size as usize;

        let data = &self.bytes[section_base..section_base + section_offset];
        let string_data = &data[offset..];

        let c_str = core::ffi::CStr::from_bytes_until_nul(string_data).ok()?;

        c_str.to_str().ok()
    }
}

impl core::fmt::Debug for Elf<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Elf")
            .field("header", &self.header)
            .field("section_headers", &self.section_headers)
            .finish()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Error {
    UnexpectedEOF,
    InvalidSignature(Signature),
    InvalidStringTable,
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let message = match self {
            Error::UnexpectedEOF => "unexpected end of file",
            Error::InvalidSignature(_) => "invalid signature",
            Error::InvalidStringTable => "invalid string table",
        };
        write!(f, "{message}")
    }
}

#[cfg(test)]
fn test_runner(_tests: &[&dyn Fn()]) {
    todo!();
}
