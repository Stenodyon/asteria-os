#![no_std]
#![no_main]

use core::arch::asm;
use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    unsafe { asm!("syscall", "hlt", options(noreturn)) };
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
