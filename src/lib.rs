//! Asteria OS Kernel

#![no_std]
#![no_main]
#![warn(missing_docs)]
#![feature(abi_x86_interrupt)]
#![feature(custom_test_frameworks)]
#![feature(debug_closure_helpers)]
#![feature(naked_functions)]
#![feature(panic_info_message)]
#![feature(pointer_is_aligned)]
#![feature(step_trait)]
#![test_runner(crate::test_runner)]

/// Performs compile-time assertions.
#[macro_export]
macro_rules! static_assert {
    ($condition:expr) => {
        const _: () = core::assert!($condition);
    };
}

/// ACPI structures and helpers.
pub mod acpi;
pub mod addr;
/// Memory allocators.
pub mod allocs;
/// Manages a linear bitmap (useful for allocators).
pub mod bitmap;
pub mod console;
/// Helpers for turning bytes into structs.
pub mod deserialization;
/// EFI structures and helpers.
pub mod efi;
pub mod geometry;
/// Linked-List data structure.
pub mod linked_list;
mod log;
pub mod multiboot;
pub mod ppm;
pub mod serial;
pub mod spinlock;
/// Kernel stack allocation.
pub mod stack;
/// System call handler.
mod syscall;
/// Thread abstractions and management.
pub mod thread;
/// Bump allocator for virtual space.
pub mod vspace;
/// x86_64 architecture abstractions.
pub mod x86_64;

use core::arch::asm;
use core::panic::PanicInfo;

use addr::{Addr, AddressRange, Phys, Virt};
use console::Console;
use elf::section::Section;
use elf::Elf;
use geometry::Vec2u;
use log::Logger;
use multiboot::{MultibootInfo, MultibootTag};
use ppm::PPM;
use serial::init_serial;
use stack::KernelStackAllocator;
use syscall::setup_syscall_handler;
use x86_64::paging::{AddressSpace, PageFrameAllocator, PAGE_SIZE};
use x86_64::{gdt, idt, interrupts, PrivilegeLevel};

use crate::thread::storage::ThreadStorage;
use crate::thread::Thread;
use crate::x86_64::gdt::IST;

/// Base virtual address mapped to physical address 0x0
pub const KERNEL_BASE: u64 = 0xffffffffc0000000;

extern "C" {
    /// Symbol created by the linker (see `bootloader_src/linker.ld`) that points just past the
    /// very last byte of the kernel.
    static _kernel_end: core::ffi::c_void;
}

/// Returns the physical address to the byte just past the end of the kernel.
pub fn kernel_end() -> Addr<Phys> {
    Addr::phys(unsafe { &_kernel_end } as *const _ as u64)
}

/// Cute possum image to test drawing images to the framebuffer.
const POSSUM: &'static [u8] = include_bytes!("possum.ppm");

const TEST_ELF: &'static [u8] = include_bytes!("../target/x86_64-asteria/debug/hello_world");

/// Function called by the `print` and `println` macros. Can be changed to redirect print output.
pub static mut _PRINT: fn(core::fmt::Arguments) = serial::_print;

/// Entry point to the kernel.
#[no_mangle]
pub extern "C" fn kernel_entry(multiboot_info: MultibootInfo) -> ! {
    if init_serial().is_err() {
        unsafe { asm!("hlt", options(noreturn)) };
    }

    Logger::init();

    gdt::load_gdt();
    interrupts::set_interrupt_handlers();
    x86_64::paging::setup_kernel_address_space();

    let memory_map = multiboot_info.memory_map().expect("No memory map");
    println!("{memory_map:#?}");
    PageFrameAllocator::init(&multiboot_info);
    KernelStackAllocator::init();

    println!(
        "Kernel range: {}..{}",
        Addr::phys(1024 * 1024),
        kernel_end()
    );

    // Setup interrupt stack
    let interrupt_stack = KernelStackAllocator::alloc();
    unsafe { IST.ist[0] = interrupt_stack.stack_top() };

    // Setup a new kernel stack.
    let kernel_stack = KernelStackAllocator::alloc();
    let stack_top = kernel_stack.stack_top();
    println!("Setting up kernel stack at {}", stack_top);
    unsafe {
        asm!(
            "mov rsp, {x}",
            "call kernel_main",
            in("rdi") core::mem::transmute::<_, u64>(multiboot_info),
            x = in(reg) stack_top.value(),
            options(noreturn)
        );
    }
}

/// Kernel main function.
#[no_mangle]
pub extern "C" fn kernel_main(multiboot_info: MultibootInfo) -> ! {
    println!("Entered kernel_main({multiboot_info:?})");
    let framebuffer = multiboot_info.framebuffer().expect("No framebuffer :(");
    println!("Got framebuffer!");
    let framebuffer = unsafe { framebuffer.move_to_virtual_space() };
    let possum = PPM::from_bytes(POSSUM).expect("Could not load POSSUM :(");
    framebuffer.blit(&possum, Vec2u::zero(), None);
    Console::init(framebuffer);

    println!("Hi? Hello?");
    println!(
        "size_of<ThreadStorage>() = {}",
        core::mem::size_of::<ThreadStorage>()
    );

    ThreadStorage::init();

    setup_syscall_handler();

    println!("Asteria OS v0.1");

    for tag in multiboot_info.tags() {
        match tag {
            MultibootTag::BasicMemoryInfo {
                mem_lower,
                mem_upper,
            } => {
                dbg_println!("Basic Memory Info: [{mem_lower:x}..{mem_upper:x}]");
            }

            _ => dbg_println!("{tag:#?}"),
        }
    }
    PageFrameAllocator::reclaim_multiboot_area(multiboot_info);

    println!("Hello world!!");

    let elf = Elf::from_bytes(TEST_ELF).expect("could not read elf");

    let longest_name = elf
        .sections()
        .into_iter()
        .map(|section| section.name().map(str::len).unwrap_or(0))
        .max()
        .unwrap_or(0);

    for section in elf.sections() {
        match section {
            Section::Generic {
                name,
                section_type,
                flags,
                ..
            } => {
                let name = name.unwrap_or("");
                println!("{name:>longest_name$} {section_type:?}");
                println!("{:>longest_name$} {flags}", "");
                println!();

                //dbg_println!("{section:#?}");
            }

            Section::SymbolTable(symbol_table) => {
                for symbol in symbol_table {
                    dbg_println!("{symbol:#?}");
                }
            }
        }
    }

    run_elf(&elf);
}

fn run_elf(elf: &Elf) -> ! {
    let mut address_space = AddressSpace::new();

    for (address, data) in elf.sections().into_iter().filter_map(is_to_be_loaded) {
        load_section(address, data, &mut address_space);
    }

    println!("ELF file loaded without errors");

    let entry_point = Addr::virt(elf.header.e_entry);
    let thread = Thread::new(address_space, entry_point, PrivilegeLevel::Ring3);
    thread.resume();
}

fn is_to_be_loaded<'a>(section: Section<'a>) -> Option<(Addr<Virt>, &'a [u8])> {
    match section {
        Section::Generic {
            flags, addr, data, ..
        } if flags.alloc() => {
            let address = addr.expect("alloc section must have an address field");
            Some((Addr::virt(address), data))
        }

        _ => None,
    }
}

fn load_section(mut address: Addr<Virt>, mut data: &[u8], process_space: &mut AddressSpace) {
    // Not page-aligned, need some special casing to copy bytes at the right offets.
    if address.value().rem_euclid(PAGE_SIZE as u64) != 0 {
        let page_offset = (address.value() as usize).rem_euclid(PAGE_SIZE);
        let bytes_to_copy = (PAGE_SIZE - page_offset).min(data.len());
        let virt_page = address.page();
        let phys_page = PageFrameAllocator::alloc().unwrap();

        let dst_bytes: &mut [u8] = unsafe {
            phys_page
                .base()
                .offset(page_offset as i64)
                .to_slice_mut(bytes_to_copy)
        };
        dst_bytes.copy_from_slice(&data[..bytes_to_copy]);
        process_space
            .map_pages(virt_page, phys_page, PrivilegeLevel::Ring3)
            .unwrap();

        address = address.aligned_to_next_page();
        data = &data[bytes_to_copy..];
    }

    // Here `address` is guaranteed to be page-aligned

    let address_range = AddressRange::from_base_and_length(address, data.len());
    for (virt_page, data) in address_range.page_range().zip(data.chunks(PAGE_SIZE)) {
        let phys_page = PageFrameAllocator::alloc().unwrap();
        let dst_bytes = &mut unsafe { phys_page.bytes_mut() }[..data.len()];
        dst_bytes.copy_from_slice(data);
        process_space
            .map_pages(virt_page, phys_page, PrivilegeLevel::Ring3)
            .unwrap();
    }
}

/// Intentionally overflows the stack, to verify the implementation of stack overflow detection.
#[allow(unconditional_recursion, dead_code)]
fn stack_overflow() {
    stack_overflow();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("KERNEL PANIC!!!");

    if let Some(location) = info.location() {
        print!("{}:{}", location.file(), location.line());
    }

    if let Some(args) = info.message() {
        println!(": {}", args);
    } else {
        println!();
    }

    if let Some(s) = info.payload().downcast_ref::<&str>() {
        println!("\t{s:?}");
    }

    unsafe { asm!("hlt", options(noreturn)) };
}

#[cfg(test)]
fn test_runner(_tests: &[&dyn Fn()]) {
    todo!();
}
