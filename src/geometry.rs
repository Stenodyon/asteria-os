//! Geometric primitives and operations, mostly in 2D.

mod rect;
mod vec;

pub use rect::*;
pub use vec::*;
