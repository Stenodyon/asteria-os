use core::arch::asm;

use crate::println;
use crate::x86_64::syscall::{enable_syscalls, set_syscall_handler};

#[no_mangle]
static mut THREAD_RSP: u64 = 0;

pub fn setup_syscall_handler() {
    enable_syscalls();
    unsafe { set_syscall_handler(syscall_entry) };
}

#[naked]
#[no_mangle]
unsafe extern "C" fn syscall_entry() {
    // RSP is *NOT* saved.
    // Return address in RCX.
    // rFLAGS in R11.
    // r12-15 are callee-saved
    asm!(
        "cli",
        "mov [THREAD_RSP], rsp",
        // TODO: Set RSP to kernel stack.
        "mov rsp, [KERNEL_STACK]",
        "push rax",
        "push rbx",
        "push rcx", // RIP
        "push rdx",
        "push rdi",
        "push rsi",
        "push rbp",
        "push [THREAD_RSP]",
        "push r8",
        "push r9",
        "push r10",
        "push r11",
        //"sub rsp, 5 * 8", // R11 is restroyed, r12-15 are callee-saved.
        "call handle_syscall",
        "pop r11",
        "pop r10",
        "pop r9",
        "pop r8",
        "pop qword ptr [THREAD_RSP]",
        "pop rbp",
        "pop rsi",
        "pop rdi",
        "pop rdx",
        "pop rcx",
        "pop rbx",
        "pop rax",
        "mov rsp, [THREAD_RSP]",
        // enable interrupts?
        "sysret",
        options(noreturn)
    );
}

#[no_mangle]
extern "C" fn handle_syscall(register_context: SyscallRegisters) {
    println!("SYSCALL!\n{register_context:#?}");
}

#[derive(Debug)]
#[repr(C)]
struct SyscallRegisters {
    r11: u64,
    r10: u64,
    r9: u64,
    r8: u64,
    rsp: u64,
    rbp: u64,
    rsi: u64,
    rdi: u64,
    rdx: u64,
    rip: u64,
    rbx: u64,
    rax: u64,
}
