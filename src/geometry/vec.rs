use core::ops::{Add, Mul, Sub};

/// Positive integer 2D vector.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Vec2u {
    /// x coordinate of the vector.
    pub x: usize,
    /// y coordinate of the vector.
    pub y: usize,
}

impl Vec2u {
    /// Returns a new vector with the given x and y values.
    pub fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }

    /// Returns a vector with zero in both coordinates.
    pub fn zero() -> Self {
        Self { x: 0, y: 0 }
    }
}

impl Add for Vec2u {
    type Output = Vec2u;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub for Vec2u {
    type Output = Vec2u;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Mul<usize> for Vec2u {
    type Output = Vec2u;

    fn mul(self, rhs: usize) -> Self::Output {
        Self {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}
