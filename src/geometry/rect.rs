use super::Vec2u;

/// A 2D rectangle in positive integer units.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Rect {
    /// The coordinates of the top-left corner of the rectangle.
    pub base: Vec2u,
    /// The size of the rectangle.
    pub size: Vec2u,
}

impl Rect {
    /// Creates a new rectangle with the given base and size.
    pub fn new(base: Vec2u, size: Vec2u) -> Self {
        Self { base, size }
    }

    /// Derives a new rectangle with the same base but the given size.
    pub fn with_size(self, size: Vec2u) -> Self {
        Self { size, ..self }
    }

    /// Computes the intersection (if it exists) of this rectangle with `other`.
    pub fn intersection(self, other: Self) -> Option<Self> {
        let base_x = self.base.x.max(other.base.x);
        let base_y = self.base.y.max(other.base.y);

        let max_x = (self.base.x + self.size.x).min(other.base.x + other.size.x);
        let max_y = (self.base.y + self.size.y).min(other.base.y + other.size.y);

        if max_x < base_x || max_y < base_y {
            return None;
        }

        let base = Vec2u::new(base_x, base_y);
        let max = Vec2u::new(max_x, max_y);
        let size = max - base;

        let rect = Rect { base, size };
        Some(rect)
    }
}
