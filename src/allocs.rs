use core::fmt::Debug;
use core::mem::ManuallyDrop;
use core::ops::{Deref, DerefMut};

/// Allocator with a fixed-size pool of elements.
pub mod fixed;

/// The allocator interface.
pub trait Allocator<T>: Sized {
    /// The type of errors that can arise when allocating.
    type Error: Debug;

    /// Allocates a value from memory.
    fn alloc(&self, value: T) -> Result<Box<T, Self>, Self::Error>;

    /// Frees the space taken by the value.
    fn free(&self, item: *mut T);
}

/// Boxed value.
pub struct Box<'alloc, T: 'static, Alloc: Allocator<T>> {
    inner: *mut T,
    allocator: &'alloc Alloc,
}

impl<T, Alloc: Allocator<T>> Box<'_, T, Alloc> {
    /// Leaks the value within the box and returns a pointer to it.
    pub fn leak(self) -> *mut T {
        let ptr = self.inner;
        let _ = ManuallyDrop::new(self);
        ptr
    }
}

impl<'alloc, T, Alloc: Allocator<T>> Drop for Box<'alloc, T, Alloc> {
    fn drop(&mut self) {
        self.allocator.free(self.inner);
    }
}

impl<T, Alloc: Allocator<T>> Deref for Box<'_, T, Alloc> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.inner }
    }
}

impl<T, Alloc: Allocator<T>> DerefMut for Box<'_, T, Alloc> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.inner }
    }
}
