//! Framebuffer-based console.

use core::fmt::Write;
use core::mem::MaybeUninit;

use crate::geometry::{Rect, Vec2u};
use crate::multiboot::Framebuffer;
use crate::ppm::PPM;
use crate::spinlock::Mutex;
use crate::_PRINT;

const FONT_DATA: &'static [u8] = include_bytes!("font.ppm");
const FIRST_GLYPH: u8 = 32;
const FONT_STRIDE: usize = 18;
const FONT_SIZE: usize = 16;

/// On-screen console that directly uses the framebuffer.
#[derive(Debug)]
pub struct Console {
    fb: Framebuffer,
    font: PPM<'static>,

    cursor: Vec2u,
}

impl Console {
    /// Initializes the global console.
    pub fn init(fb: Framebuffer) {
        unsafe {
            CONSOLE.write(Mutex::new(Self::new(fb)));
            _PRINT = _print;
        }
    }

    /// Creates a new console that will draw to the framebuffer.
    pub fn new(fb: Framebuffer) -> Self {
        let font = PPM::from_bytes(&FONT_DATA).expect("Could not load font");

        Self {
            fb,
            font,
            cursor: Vec2u::zero(),
        }
    }

    /// Write a single character at the current cursor position.
    pub fn putchar(&mut self, c: u8) {
        if c == b'\n' {
            self.cursor.x = 0;
            self.newline();
            return;
        }

        if c == b'\r' {
            self.cursor.x = 0;
            return;
        }

        let src_rect = glyph_rect(c).unwrap_or_else(|| glyph_rect(b'?').unwrap());

        let dst = self.cursor * FONT_SIZE;
        self.fb.blit(&self.font, dst, Some(src_rect));

        self.cursor.x += 1;
        if self.cursor.x >= self.char_width() {
            self.cursor.x = 0;
            self.newline();
        }
    }

    /// Puts the cursor at the beginning of the next line. Will scroll the console up if necessary.
    pub fn newline(&mut self) {
        self.cursor.y += 1;
        if self.cursor.y >= self.char_height() {
            self.cursor.y = self.char_height() - 1;
            self.fb.scroll_up(FONT_SIZE);
        }
    }

    /// Writes a string to the console, wrapping it if necessary, and scrolling up if necessary.
    pub fn write_str(&mut self, s: &str) {
        for c in s.chars() {
            self.putchar(if c.is_ascii() { c as u8 } else { b'?' })
        }
    }

    /// Width of the console in characters.
    fn char_width(&self) -> usize {
        self.fb.width() / FONT_SIZE
    }

    /// Height of the console in characters.
    fn char_height(&self) -> usize {
        self.fb.height() / FONT_SIZE
    }
}

impl Write for Console {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.write_str(s);
        Ok(())
    }
}

/// Returns the source rectangle in the font image for a given ASCII glyph.
fn glyph_rect(glyph: u8) -> Option<Rect> {
    if glyph < FIRST_GLYPH {
        return None;
    }

    let rect = Rect::new(
        Vec2u::new(FONT_STRIDE * (glyph - FIRST_GLYPH) as usize, 0),
        Vec2u::new(FONT_SIZE, FONT_SIZE),
    );
    Some(rect)
}

/// Global console instance.
static mut CONSOLE: MaybeUninit<Mutex<Console>> = MaybeUninit::uninit();

#[doc(hidden)]
pub fn _print(args: core::fmt::Arguments) {
    crate::serial::_print(args);

    unsafe { CONSOLE.assume_init_mut() }
        .lock()
        .write_fmt(args)
        .unwrap();
}

/// Prints formatted strings to the console and the serial port.
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => (unsafe { $crate::_PRINT(format_args!($($arg)*)) });
}

/// Same as [`print`] but will add a newline at the end.
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}
