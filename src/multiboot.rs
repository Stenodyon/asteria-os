//! Helpers for reading the boot information provided by the multiboot2 protocol.

use core::mem::size_of;

mod framebuffer;
mod memory_map;

pub use framebuffer::*;
pub use memory_map::*;

use crate::acpi;
use crate::addr::{Addr, Phys};
use crate::deserialization::Bytes;
use crate::efi::memory_map::MemoryDescriptor;
use crate::efi::SystemTable;

/// Type for accessing the boot information given to us by the multiboot2 protocol.
#[repr(transparent)]
pub struct MultibootInfo(*const Header);

impl MultibootInfo {
    /// Returns the size in bytes of the multiboot info structure.
    pub fn size(&self) -> usize {
        unsafe { (*self.0).total_size as usize }
    }

    /// Returns the physical address of the multiboot info structure.
    pub fn addr(&self) -> Addr<Phys> {
        Addr::phys(self.0 as u64)
    }

    /// Returns an iterator over the tags contained in the boot information structure.
    pub fn tags(&self) -> Tags {
        unsafe {
            let total_size = (*self.0).total_size as usize - size_of::<Header>();
            let start = (self.0 as *const u8).add(size_of::<Header>());
            let slice = core::slice::from_raw_parts(start, total_size);
            Tags(Bytes::new(slice))
        }
    }

    /// Returns the memory map from the multiboot info if it is present, otherwise returns `None`.
    pub fn memory_map(&self) -> Option<MemoryMap> {
        for tag in self.tags() {
            if let MultibootTag::MemoryMap(memory_map) = tag {
                return Some(memory_map);
            }
        }
        None
    }

    /// Returns the framebuffer from the multiboot info if it is present, otherwise returns `None`.
    pub fn framebuffer(&self) -> Option<Framebuffer> {
        for tag in self.tags() {
            if let MultibootTag::Framebuffer(framebuffer_info) = tag {
                return Some(framebuffer_info);
            }
        }
        None
    }
}

impl core::fmt::Debug for MultibootInfo {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "MultibootInfo({:?})", self.0)
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
struct Header {
    total_size: u32,
    reserved: u32,
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
struct Tag {
    tag_type: u32,
    size: u32,
}

/// Iterator over the tags in the boot information structure.
pub struct Tags(Bytes<'static>);

impl Iterator for Tags {
    type Item = MultibootTag;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_empty() {
            return None;
        }

        let initial_state = self.0;

        let tag_type = self.0.read_u32().unwrap();
        let tag_size = self.0.read_u32().unwrap();

        let tag = match tag_type {
            1 => {
                let command_line = self.0.read_string().unwrap();

                MultibootTag::BootCommandLine { command_line }
            }

            2 => {
                let mod_start = self.0.read_u32().unwrap();
                let mod_end = self.0.read_u32().unwrap();
                let string = self.0.read_string().unwrap();

                MultibootTag::Modules {
                    mod_start,
                    mod_end,
                    string,
                }
            }

            4 => {
                let mem_lower = self.0.read_u32().unwrap();
                let mem_upper = self.0.read_u32().unwrap();

                MultibootTag::BasicMemoryInfo {
                    mem_lower,
                    mem_upper,
                }
            }

            6 => {
                let entry_size = self.0.read_u32().unwrap() as usize;
                let _entry_version = self.0.read_u32().unwrap();

                let base_address = self.0.ptr();
                let data =
                    unsafe { core::slice::from_raw_parts(base_address, tag_size as usize - 16) };

                MultibootTag::MemoryMap(MemoryMap::new(entry_size, data))
            }

            8 => {
                if let Ok(info) = read_framebuffer_info(&mut self.0) {
                    MultibootTag::Framebuffer(info)
                } else {
                    MultibootTag::Error(tag_type)
                }
            }

            12 => {
                let pointer = self.0.read_u64().unwrap();

                MultibootTag::EFI64SystemTablePointer(unsafe { Addr::phys(pointer).to_ref() })
            }

            15 => {
                let rsdp = unsafe { *(self.0.ptr() as *const acpi::RSDPv2) };

                MultibootTag::ACPI2_0(rsdp)
            }

            17 => {
                let map_size = self.0.read_u32().unwrap() as usize;
                let _descritpor_size = self.0.read_u32().unwrap();
                let _descriptor_version = self.0.read_u32().unwrap();

                self.0.align_to(8);

                let map_base = Addr::phys(self.0.ptr() as u64);
                let descriptor_count = map_size.div_euclid(size_of::<MemoryDescriptor>());

                MultibootTag::EfiMemoryMap(unsafe { map_base.to_slice(descriptor_count) })
            }

            _ => MultibootTag::Unknown(tag_type),
        };

        self.0 = initial_state;
        self.0.skip(tag_size as usize);
        self.0.align_to(8);

        Some(tag)
    }
}

/// Various tags that the multiboot protocol can provide.
#[derive(Debug, Copy, Clone)]
pub enum MultibootTag {
    // type 1
    /// Command-line argument given to the booted executable.
    BootCommandLine {
        /// The command line string.
        command_line: &'static str,
    },

    // type 2
    /// Indicates a boot module that was loaded alongside the kernel.
    Modules {
        /// Physical address of the start of the boot module.
        mod_start: u32,
        /// Physical address of the end of the boot module.
        mod_end: u32,
        /// Arbitrary string associated with this boot module. Its semantics is specific to the OS.
        string: &'static str,
    },

    // type 4
    /// Contains basic amounts of memory.
    BasicMemoryInfo {
        /// Amount of lower memory in kilobytes (starting at address 0).
        ///
        /// Max value is 640.
        mem_lower: u32,
        /// Amount of higher memory in kilobytes (starting at 1MiB).
        mem_upper: u32,
    },

    // type 6
    /// Holds a map of memory.
    MemoryMap(MemoryMap<'static>),

    // type 8
    /// Holds the framebuffer leftover after booting.
    Framebuffer(Framebuffer),

    // type 12
    /// Pointer to the EFI System Table.
    EFI64SystemTablePointer(&'static SystemTable<'static>),

    // type 15
    /// ACPI RSDP version 2.
    ACPI2_0(acpi::RSDPv2),

    // type 17
    /// Memory map left from EFI.
    EfiMemoryMap(&'static [MemoryDescriptor]),

    /// Unknown tag.
    Unknown(u32),

    /// Error when reading the given tag.
    Error(u32),
}
