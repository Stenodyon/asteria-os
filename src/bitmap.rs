/// Linear bitmap (useful for allocators).
#[derive(Debug)]
pub struct Bitmap {
    inner: &'static mut [u8],
}

impl Bitmap {
    /// Creates a new bitmap using the provided memory region.
    pub fn new(region: &'static mut [u8]) -> Self {
        Self { inner: region }
    }

    /// Fills the bitmap with the given value.
    pub fn fill(&mut self, value: bool) {
        let value = if value { 0xff } else { 0 };
        self.inner.fill(value);
    }

    /// Finds the first bit set to 1 and returns its index. Returns None if all bits are zero.
    pub fn find_first(&self) -> Option<usize> {
        for (byte_index, byte) in self.inner.iter().copied().enumerate() {
            if byte == 0 {
                continue;
            }

            let bit = byte.trailing_zeros() as usize;

            return Some(byte_index * 8 + bit);
        }

        None
    }

    /// Sets the state of the bit at the given index.
    pub fn set(&mut self, index: usize, value: bool) {
        let byte = index.div_euclid(8);
        let bit = index.rem_euclid(8);

        let mask = 1 << bit;
        if value {
            self.inner[byte] |= mask;
        } else {
            self.inner[byte] &= !(mask);
        }
    }

    /// Returns the number of ones in the entire bitmap.
    pub fn count(&self) -> usize {
        self.inner
            .iter()
            .copied()
            .map(u8::count_ones)
            .map(|x| x as usize)
            .sum()
    }
}
