use core::mem::{align_of, size_of};

use crate::allocs::Allocator;
use crate::bitmap::Bitmap;
use crate::spinlock::Mutex;

type Box<'a, T> = super::Box<'a, T, FixedAlloc<T>>;

/// Allocator for T with a fixed region of memory.
pub struct FixedAlloc<T> {
    inner: Mutex<FixedAllocImpl<T>>,
}

impl<T> FixedAlloc<T> {
    /// Creates a new allocator over the given memory region.
    pub fn new(region: &'static mut [u8]) -> Self {
        Self {
            inner: Mutex::new(FixedAllocImpl::new(region)),
        }
    }
}

impl<T> Allocator<T> for FixedAlloc<T> {
    type Error = AllocError;

    fn alloc(&self, value: T) -> Result<Box<T>, Self::Error> {
        let alloc = self.inner.lock().alloc(value)?;
        let result = Box {
            inner: alloc,
            allocator: &self,
        };
        Ok(result)
    }

    fn free(&self, value: *mut T) {
        self.inner.lock().free(value);
    }
}

struct FixedAllocImpl<T> {
    bitmap: Bitmap,
    base: *mut T,
    length: usize,
}

impl<T> FixedAllocImpl<T> {
    fn new(region: &'static mut [u8]) -> Self {
        assert!((region.as_ptr() as *const T).is_aligned());

        // The region is divided in two, the start of the region contains the bitmap, and the rest
        // contains the storage for allocated values.
        // | bitmap | (alignment padding) | T storage |

        // Max quantity of T that can fit in the region.
        let max_fit = region.len().div_euclid(size_of::<T>());
        // Size of the bitmap to map max_fit values.
        let bitmap_length = (max_fit + 8 - 1).div_euclid(8);
        let (bitmap_region, data_region) = region.split_at_mut(bitmap_length);
        let bitmap = Bitmap::new(bitmap_region);

        let data_start = unsafe {
            data_region
                .as_mut_ptr()
                .add(data_region.as_mut_ptr().align_offset(align_of::<T>()))
                .cast::<T>()
        };
        let data_end = data_region.as_ptr_range().end.cast::<T>();
        let data_len = (data_end as usize - data_start as usize).div_euclid(size_of::<T>());

        Self {
            bitmap,
            base: data_start,
            length: data_len,
        }
    }

    fn alloc(&mut self, value: T) -> Result<*mut T, AllocError> {
        let index = self.bitmap.find_first().ok_or(AllocError::NoMoreSpace)?;
        self.bitmap.set(index, false);
        let ptr = unsafe { self.base.add(index) };
        unsafe { *ptr = value };
        Ok(ptr)
    }

    fn free(&mut self, value: *mut T) {
        assert!(value as u64 >= self.base as u64);
        let index = (value as usize - self.base as usize) / size_of::<T>();
        assert!(index < self.length);
        unsafe { value.drop_in_place() };
        self.bitmap.set(index, true);
    }
}

/// Error returned when allocating failed.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum AllocError {
    /// There is no more space left in the data region.
    NoMoreSpace,
}
