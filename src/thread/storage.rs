use core::mem::MaybeUninit;

use super::Thread;

/// Generational index that uniquely identifies a thread.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ThreadId {
    generation: usize,
    index: usize,
}

impl core::fmt::Display for ThreadId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "ThreadId({}:{})", self.generation, self.index)
    }
}

#[derive(Debug)]
struct StorageEntry {
    generation: usize,
    thread: Option<Thread>,
}

impl Default for StorageEntry {
    fn default() -> Self {
        Self {
            generation: 0,
            thread: None,
        }
    }
}

/// The global Thread Store where thread data is placed and managed.
pub static mut THREAD_STORAGE: MaybeUninit<ThreadStorage> = MaybeUninit::uninit();

const THREAD_STORAGE_SIZE: usize = 512;

/// Storage for data. ThreadId contains indices into this storage.
pub struct ThreadStorage([StorageEntry; THREAD_STORAGE_SIZE]);

impl ThreadStorage {
    /// Initializes the thread store. *Must* be called before any operations on THREAD_STORAGE.
    pub fn init() {
        // Little switcharoo to avoid Rust trying to drop uninitialized values.
        let no_drop = unsafe {
            core::mem::transmute::<
                &mut [StorageEntry; THREAD_STORAGE_SIZE],
                &mut [MaybeUninit<StorageEntry>; THREAD_STORAGE_SIZE],
            >(&mut THREAD_STORAGE.assume_init_mut().0)
        };
        no_drop.fill_with(|| MaybeUninit::new(StorageEntry::default()));
    }

    /// Returns the number of free slots in thread storage.
    pub fn free_slots(&self) -> usize {
        self.0.iter().filter(|entry| entry.thread.is_none()).count()
    }

    fn find_free_slot(&self) -> Option<usize> {
        self.0.iter().position(|entry| entry.thread.is_none())
    }

    /// Adds the given thread to storage and returns its id for lookup. Returns an error if the
    /// storage is full.
    pub fn add_thread(&mut self, mut thread: Thread) -> Result<ThreadId, ()> {
        let slot = self.find_free_slot().ok_or(())?;
        let entry = &mut self.0[slot];
        entry.generation += 1;
        let id = ThreadId {
            generation: entry.generation,
            index: slot,
        };
        thread.kernel_stack().set_thread_id(id);
        entry.thread = Some(thread);
        Ok(id)
    }

    /// Looks up the thread by id. Returns None if the ID is invalid.
    pub fn get(&self, thread_id: ThreadId) -> Option<&Thread> {
        let entry = &self.0[thread_id.index];
        if entry.generation != thread_id.generation {
            return None;
        }

        entry.thread.as_ref()
    }

    /// Looks up the thread by id. Returns None if the ID is invalid.
    pub fn get_mut(&mut self, thread_id: ThreadId) -> Option<&mut Thread> {
        let entry = &mut self.0[thread_id.index];
        if entry.generation != thread_id.generation {
            return None;
        }

        entry.thread.as_mut()
    }
}
