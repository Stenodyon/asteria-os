use core::ops::RangeInclusive;

use crate::addr::{Addr, Phys, Virt};
use crate::x86_64::paging::PAGE_SIZE;

/// Describes a memory region.
#[derive(Debug)]
#[repr(C)]
pub struct MemoryDescriptor {
    region_type: MemoryType,
    phys_addr: Addr<Phys>,
    virt_addr: Addr<Virt>,
    number_of_pages: u64,
    attribute: RegionAttribute,
}

impl MemoryDescriptor {
    /// Returns the physical address range of the region.
    pub fn phys_addr_range(&self) -> RangeInclusive<Addr<Phys>> {
        let length = self.number_of_pages as usize * PAGE_SIZE;
        let end = self.phys_addr.offset(length as i64 - 1);
        self.phys_addr..=end
    }
}

impl core::fmt::Display for MemoryDescriptor {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let range = self.phys_addr_range();
        write!(
            f,
            "{}..{} ({}) | {}",
            range.start(),
            range.end(),
            range.end() - range.start(),
            self.region_type,
        )
    }
}

/// Represents the raw attributes field of a memory region.
///
/// See the UEFI 2.6 Specifications, pages 158-159.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct RegionAttribute(u64);

impl RegionAttribute {
    /// The memory region supports being configured as not cacheable.
    pub fn supports_no_cache(&self) -> bool {
        (self.0 & 0x1) != 0
    }

    /// The memory region supports being configured as write combining.
    pub fn supports_write_combining(&self) -> bool {
        (self.0 & 0x2) != 0
    }

    /// The memory region supports being configured as cacheable with a "writethrough" policy.
    pub fn supports_writethrough(&self) -> bool {
        (self.0 & 0x4) != 0
    }

    /// The memory region supports being configured as cacheable with a "write back" policy.
    pub fn supports_writeback(&self) -> bool {
        (self.0 & 0x8) != 0
    }

    /// The memory region supports being configured as not cacheable, exported and supports the
    /// "fetch and add" semaphore mechanism.
    pub fn supports_no_cache_semaphore(&self) -> bool {
        (self.0 & 0x10) != 0
    }

    /// The memory region supports being configured as write-protected by system hardware.
    pub fn supports_write_protection(&self) -> bool {
        (self.0 & 0x1000) != 0
    }

    /// The memory region supports being configured as read-protected by system hardware.
    pub fn supports_read_protection(&self) -> bool {
        (self.0 & 0x2000) != 0
    }

    /// The memory region supports being configured so it is protected by system hardware from
    /// executing code.
    pub fn supports_execution_protection(&self) -> bool {
        (self.0 & 0x4000) != 0
    }

    /// The memory region refers to persistent memory.
    pub fn is_persistent(&self) -> bool {
        (self.0 & 0x8000) != 0
    }

    /// The memory region provides higher reliability relative to other memory in the system.
    pub fn is_more_reliable(&self) -> bool {
        (self.0 & 0x10000) != 0
    }

    /// The memory region supports making this memory range read-only by system hardware.
    pub fn supports_read_only(&self) -> bool {
        (self.0 & 0x20000) != 0
    }
}

/// Possible types for a memory region according to the EFI specification.
///
/// See UEFI 2.6 Specifications pages 150-152
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u32)]
#[non_exhaustive]
pub enum MemoryType {
    /// Not usable.
    ReservedMemory,
    /// The code portions of a loaded UEFI application.
    /// After `ExitBootServices()` may be used freely.
    LoaderCode,
    /// The data portions of a loaded UEFI application.
    /// After `ExitBootServices()` may be used freely.
    LoaderData,
    /// The code portions of a loaded UEFI Boot Service Driver.
    /// After `ExitBootServices()` may be used freely.
    BootServicesCode,
    /// The data portions of a loaded UEFI Boot Service Driver.
    /// After `ExitBootServices()` may be used freely.
    BootServicesData,
    /// The code portions of a loaded UEFI Runtime Driver. Must be preserved.
    RuntimeServicesCode,
    /// The data portions of a loaded UEFI Runtime Driver. Must be preserved.
    RuntimeServicesData,
    /// Free memory.
    ConventionalMemory,
    /// Memory in which errors have been detected.
    UnusableMemory,
    /// Memory that holds the ACPI tables. Must be preserved until ACPI is enabled.
    /// Once ACPI is enabled, it may be used freely.
    ACPIReclaimMemory,
    /// Address space reserved for use by the firmware. Must be preserved.
    ACPIMemoryNVS,
    /// Used by system firmware to request memory-mapped IO be mapped to a virtual address for use
    /// by the EFI runtime services. Must not be used by the OS.
    MemoryMappedIO,
    /// System memory-mapped IO region. Must not be used by the OS.
    MemoryMappedIOPortSpace,
    /// Address space reserved by the firmware for code that is part of the processor. Must be
    /// preserved.
    PalCode,
    /// A memory region that operates as `EfiConventionalMemory` but also happens to support
    /// byte-addressable non-volatility.
    PersistentMemory,
}

impl core::fmt::Display for MemoryType {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let s = match self {
            MemoryType::ReservedMemory => "Reserved memory",
            MemoryType::LoaderCode => "Loader code",
            MemoryType::LoaderData => "Loader data",
            MemoryType::BootServicesCode => "Boot services code",
            MemoryType::BootServicesData => "Boot services data",
            MemoryType::RuntimeServicesCode => "Runtime services code",
            MemoryType::RuntimeServicesData => "Runtime services data",
            MemoryType::ConventionalMemory => "Conventional memory",
            MemoryType::UnusableMemory => "Unusable memory",
            MemoryType::ACPIReclaimMemory => "ACPI reclaim memory",
            MemoryType::ACPIMemoryNVS => "ACPI NVS memory",
            MemoryType::MemoryMappedIO => "Memory-mapped IO",
            MemoryType::MemoryMappedIOPortSpace => "Memory-mapped IO port space",
            MemoryType::PalCode => "Pal code",
            MemoryType::PersistentMemory => "Persistent memory",
        };

        write!(f, "{s}")
    }
}
