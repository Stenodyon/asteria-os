use crate::addr::{Addr, Virt};
use crate::bitmap::Bitmap;
use crate::spinlock::Mutex;
use crate::thread::storage::ThreadId;
use crate::x86_64::paging::{AddressSpace, PageFrameAllocator, PageId};
use crate::x86_64::PrivilegeLevel;

/// Allocated kernel stacks are placed into virtual address space starting from this page.
pub const BASE_PAGE: PageId<Virt> = PageId::from_indices([511, 511, 508, 0]);

/// Size of a kernel stack *in pages*, *including the guard page*.
pub const STACK_SIZE: usize = 8;

static KERNEL_STACK_ALLOCATOR: Mutex<Option<KernelStackAllocator>> = Mutex::new(None);

/// Manages allocation and freeing of kernel stacks.
pub struct KernelStackAllocator {
    bitmap: Bitmap,
}

impl KernelStackAllocator {
    /// Initializes the kernel stack allocator. Requires that the page frame allocator has been
    /// initialized.
    pub fn init() {
        let mut allocator = KERNEL_STACK_ALLOCATOR.lock();
        if allocator.is_some() {
            panic!("KernelStackAllocator::init called twice");
        }

        let _ = allocator.insert(Self::new());
    }

    /// Creates a new kernel stack allocator using one page of physical memory.
    fn new() -> Self {
        // A single page should be enough to store the bitmap.
        let page = PageFrameAllocator::alloc().unwrap();
        let bytes = unsafe { page.bytes_mut() };

        let top_page = PageId::from_indices([511, 511, 511, 511]);
        // Number of pages available
        let page_count = (top_page - BASE_PAGE) as usize;
        // Number of stacks available.
        let stack_count = page_count.div_euclid(STACK_SIZE);
        let bitmap_size = (stack_count + 8 - 1).div_euclid(8);
        assert!(bitmap_size <= bytes.len());

        let mut bitmap = Bitmap::new(&mut bytes[0..bitmap_size]);
        bitmap.fill(true);

        Self { bitmap }
    }

    /// Globally allocates a new kernel stack.
    pub fn alloc() -> KernelStack {
        KERNEL_STACK_ALLOCATOR
            .lock()
            .as_mut()
            .expect("KernelStackAllocator::init must be called before calling alloc")
            .alloc_impl()
    }

    /// Allocates a new kernel stack.
    fn alloc_impl(&mut self) -> KernelStack {
        let stack_id = self.bitmap.find_first().expect("ran out of kernel stacks");
        self.bitmap.set(stack_id, false);
        let bottom_page = BASE_PAGE + stack_id * STACK_SIZE;
        let stack = KernelStack { bottom_page };

        let mut page_structure = AddressSpace::current();

        page_structure.map_guard_page(stack.guard_page()).unwrap();
        for page in stack.stack_pages() {
            let physical_page = PageFrameAllocator::alloc().unwrap();
            page_structure
                .map_pages(page, physical_page, PrivilegeLevel::Ring0)
                .unwrap();
        }

        stack
    }

    /// Frees the kernel stack.
    pub fn free(stack: KernelStack) {
        KERNEL_STACK_ALLOCATOR
            .lock()
            .as_mut()
            .expect("KernelStackAllocator::init must be called before calling free")
            .free_impl(stack)
    }

    /// Frees the kernel stack.
    fn free_impl(&mut self, stack: KernelStack) {
        let mut page_structure = AddressSpace::current();

        for page in stack.pages() {
            page_structure.unmap(page);
        }

        self.bitmap.set(self.stack_index(stack), true);
    }

    fn stack_index(&self, stack: KernelStack) -> usize {
        let page_offset = stack.bottom_page - BASE_PAGE;
        if page_offset < 0 {
            panic!("Kernel stack allocated *before* BASE_PAGE???");
        }

        (page_offset as usize).div_euclid(STACK_SIZE)
    }
}

/// Represents a kernel stack that is currently allocated.
#[derive(Debug)]
pub struct KernelStack {
    bottom_page: PageId<Virt>,
}

impl KernelStack {
    /// Returns an address pointing one byte past the top of the stack.
    pub fn stack_top(&self) -> Addr<Virt> {
        (self.bottom_page + STACK_SIZE - 1).address_range().end() + 1usize
    }

    /// Returns an address pointing to the bottom of the stack (right above the guard page).
    pub fn stack_bottom(&self) -> Addr<Virt> {
        (self.bottom_page + 1).address_range().start()
    }

    /// Returns an iterator over the pages used by this stack (including the guard page).
    pub fn pages(&self) -> impl Iterator<Item = PageId<Virt>> {
        self.bottom_page..self.bottom_page + STACK_SIZE
    }

    /// Returns the ID of the stack's guard page.
    pub fn guard_page(&self) -> PageId<Virt> {
        self.bottom_page
    }

    /// Returns an iterator over the stack's pages (*not including* the guard page).
    pub fn stack_pages(&self) -> impl Iterator<Item = PageId<Virt>> {
        self.bottom_page + 1..self.bottom_page + STACK_SIZE
    }

    /// Writes the ThreadId at the bottom of the stack.
    pub fn set_thread_id(&mut self, info: ThreadId) {
        let ptr = self.stack_bottom().as_ptr_mut::<ThreadId>();
        unsafe { *ptr = info };
    }
}

// TODO: Drop implementation to automatically free kernel stacks?

/// Alternative representation of a kernel stack as a single pointer (useful when it must be
/// accessed from assembly).
#[repr(transparent)]
pub struct KernelStackPtr(Addr<Virt>);

impl KernelStackPtr {
    /// Obtains a wrapping pointer to the top of the given kernel stack. This makes it easy to read
    /// and write the value from assembly as it is simply an 8-bytes value that points to the top
    /// of the stack.
    pub fn from(kernel_stack: &KernelStack) -> Self {
        Self(kernel_stack.stack_top())
    }

    /// Returns the bottom (guard) page of the stack.
    pub fn bottom_page(&self) -> PageId<Virt> {
        let stack_id = ((self.0 - 1).page() - BASE_PAGE).div_euclid(STACK_SIZE as isize) as usize;
        BASE_PAGE + stack_id * STACK_SIZE
    }

    /// Returns the address of the bottom of the stack (right above the guard page).
    pub fn stack_bottom(&self) -> Addr<Virt> {
        (self.bottom_page() + 1).address_range().start()
    }

    /// Returns the ThreadId that is stored at the bottom of the stack.
    pub fn thread_id(&self) -> ThreadId {
        unsafe { *self.stack_bottom().as_ptr() }
    }
}
