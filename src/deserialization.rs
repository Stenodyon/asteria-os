use core::ffi::CStr;

use crate::addr::{Addr, Phys};

/// Wrapper to turn a set of bytes into a stream of bytes from which several types can be read.
#[derive(Debug, Copy, Clone)]
pub struct Bytes<'a>(&'a [u8]);

impl<'a> Bytes<'a> {
    /// Creates a new `Bytes` structure over the given bytes.
    pub fn new(bytes: &'a [u8]) -> Self {
        Self(bytes)
    }

    /// Returns a pointer to the cursor.
    pub fn ptr(&self) -> *const u8 {
        self.0.as_ptr()
    }

    /// Returns true if there are no bytes left.
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Advances the cursor by `amount` bytes.
    pub fn skip(&mut self, amount: usize) {
        let amount = amount.min(self.0.len());
        self.0 = &self.0[amount..];
    }

    /// Aligns the cursor to a multiple of `alignment`. If the cursor is already aligned, does
    /// nothing.
    pub fn align_to(&mut self, alignment: usize) {
        let remainder = self.0.as_ptr() as usize % alignment;

        if remainder == 0 {
            return;
        }

        self.skip(alignment - remainder);
    }

    /// Reads one byte, returns none if there are no bytes left.
    pub fn read_u8(&mut self) -> Option<u8> {
        if self.is_empty() {
            return None;
        }

        let value = self.0[0];
        self.skip(1);
        Some(value)
    }

    /// Reads 4 bytes in little endian, returns None if there are not enough bytes left.
    pub fn read_u32(&mut self) -> Option<u32> {
        let bytes: [u8; 4] = (&self.0[..4]).try_into().ok()?;
        self.skip(4);
        Some(u32::from_le_bytes(bytes))
    }

    /// Reads 8 bytes in little endian, returns None if there are not enough bytes left.
    pub fn read_u64(&mut self) -> Option<u64> {
        let bytes: [u8; 8] = (&self.0[..8]).try_into().ok()?;
        self.skip(8);
        Some(u64::from_le_bytes(bytes))
    }

    /// Reads an 8-byte address in little endian. Returns None if there are not enough bytes left.
    pub fn read_ptr<T>(&mut self) -> Option<*const T> {
        self.read_u64().map(|x| x as *const T)
    }

    /// Reads an 8-byte address in little endian. Returns None if there are not enough bytes left.
    pub fn read_ptr_mut<T>(&mut self) -> Option<*mut T> {
        self.read_u64().map(|x| x as *mut T)
    }

    /// Reads an 8-byte physical address in little endian. Returns None if there are not enough
    /// bytes left.
    pub fn read_addr(&mut self) -> Option<Addr<Phys>> {
        self.read_u64().map(Addr::phys)
    }

    /// Reads a null-terminated string, returns None if it reached the end without encountering a
    /// null byte.
    pub fn read_string(&mut self) -> Option<&'a str> {
        let s = CStr::from_bytes_until_nul(self.0).ok()?.to_str().ok()?;
        self.skip(s.len() + 1);
        Some(s)
    }
}

/// Small in-place fixed-size string type.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct SizedStr<const N: usize> {
    data: [u8; N],
}

impl<const N: usize> core::fmt::Display for SizedStr<N> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", core::str::from_utf8(self.data.as_slice()).unwrap())
    }
}

impl<const N: usize> core::fmt::Debug for SizedStr<N> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "{:?}",
            core::str::from_utf8(self.data.as_slice()).unwrap()
        )
    }
}
