use core::arch::asm;

mod cpuid;
pub mod gdt;
pub mod idt;
pub mod interrupts;
/// Helpers for reading and writing Machine Specific Registers.
mod msr;
pub mod paging;
/// Wrapper around the rFLAGS register.
mod rflags;
/// Helpers for syscalls.
pub mod syscall;

pub use cpuid::*;
pub use msr::*;
pub use rflags::*;

use self::gdt::GdtSelector;
use crate::addr::{Addr, Phys};

/// Wrapper around the contents of register CR3 in order to make reading its fields easier.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
pub struct CR3(u64);

impl CR3 {
    /// Returns the base address of the level-4 page table.
    pub fn table_base_address(&self) -> Addr<Phys> {
        // Mask from bits 12 to 51
        let mask = ((1 << 52) - 1) & !((1 << 12) - 1);

        Addr::phys(self.0 & mask)
    }

    /// Sets the base physical address of the level-4 page table.
    pub fn set_table_base_address(&mut self, address: Addr<Phys>) {
        let mask = !((1 << 12) - 1);
        self.0 = (self.0 & !mask) | (address.value() & mask);
    }
}

/// Reads and returns the value of control register CR3.
#[inline(always)]
pub fn cr3() -> CR3 {
    let cr3: u64;
    unsafe { asm!( "mov {out}, cr3", out = out(reg) cr3) };
    CR3(cr3)
}

/// Sets CR3 to the given value.
#[inline(always)]
pub unsafe fn set_cr3(cr3: CR3) {
    asm!("mov cr3, {root}", root = in(reg) cr3.0);
}

/// IN instruction. Reads a byte from an IO port.
#[inline(always)]
pub unsafe fn inb(port: u16) -> u8 {
    unsafe {
        let value: u8;

        asm!(
            "in al, dx",
            in("dx") port,
            out("al") value,
        );

        value
    }
}

/// OUT instruction. Writes a byte to an IO port.
#[inline(always)]
pub unsafe fn outb(port: u16, value: u8) {
    unsafe {
        asm!(
            "out dx, al",
            in("al") value,
            in("dx") port,
        );
    }
}

/// LTR instruction. Sets up the TSS.
pub unsafe fn ltr(selector: GdtSelector) {
    asm!("ltr {x:x}", x = in(reg) selector as u32);
}

/// Represents a privilege level in the x86_64 architecture. Only levels 0 and 3 are represented as
/// levels 1 and 2 are not used in the kernel.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum PrivilegeLevel {
    /// Kernel privilege level (CPL 0).
    Ring0 = 0,

    /// Userland privilege level (CPL 3).
    Ring3 = 3,
}

/// Pops a value from the stack.
///
/// # Safety
/// This function directly affects RSP which can break *a lot* of assumptions made by the compiler.
#[inline(always)]
pub unsafe fn pop() -> u64 {
    let value: u64;
    asm!("pop {x}", x = out(reg) value);
    value
}
