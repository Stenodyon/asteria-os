//! Very basic driver for the serial port.
//!
//! Serial port registers:
//! 0 : Data register
//! 1 : Interrupt enable register
//! 2 : Interrupt identification and FIFO control
//! 3 : Line control register
//! 4 : Modem control register
//! 5 : Line status register
//! 6 : Modem status register
//! 7 : Scratch register
//!
//! One of the registers (register 3) hold what is termed the DLAB or Divisor Latch Access Bit.
//! When this bit is set, offsets 0 and 1 are mapped to the low and high bytes of the Divisor
//! register for setting the baud rate of the port. When this bit is clear, offsets 0 and 1 are
//! mapped to their normal registers.

use crate::x86_64::{inb, outb};

// COM1 port
const PORT: u16 = 0x3f8;

/// Initializes the serial driver and must be called before any use of the serial interface is
/// made.
pub fn init_serial() -> Result<(), ()> {
    // code from https://wiki.osdev.org/Serial_Ports
    unsafe {
        outb(PORT + 1, 0x00); // Disable all interrupts
        outb(PORT + 3, 0x80); // Enable DLAB (set baud rate divisor)

        // Set divisor to 3 (38400 baud)
        outb(PORT + 0, 0x03); // lo byte
        outb(PORT + 1, 0x00); // hi byte
        outb(PORT + 3, 0x03); // 8 bits, no parity, one stop bit
        outb(PORT + 2, 0xc7); // Enable FIFO, clear them, with 14-byte threshold
        outb(PORT + 4, 0x0b); // RTS/DSR set

        // Testing the serial chip by:
        // 1. Putting it in loopback mode.
        // 2. Sending a byte (0xAE).
        // 3. Checking if the exact same byte is read back from serial.
        // (4. Disable loopback mode)

        outb(PORT + 4, 0x1e); // 1. Set in loopback mode
        outb(PORT + 0, 0xae); // 2. Send test byte

        // 3. Check if the test byte is returned
        if inb(PORT + 0) != 0xae {
            return Err(());
        }

        // 4. Exit loopback, back to normal operations mode
        outb(PORT + 4, 0x0f);
    }

    Ok(())
}

#[allow(dead_code)]
fn is_transmit_empty() -> bool {
    let line_status = unsafe { inb(PORT + 5) };
    (line_status & (1 << 5)) != 0
}

/// Write a byte to the serial port. PLEASE CALL [`init_serial`] FIRST!
pub fn write_serial(value: u8) {
    // FIXME: why is that not working?
    //while is_transmit_empty() {}

    unsafe { outb(PORT, value) };
}

/// Represents the serial driver in order to implement [`core::fmt::Write`] on.
pub struct Serial;

impl core::fmt::Write for Serial {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for c in s.bytes() {
            write_serial(c);
            if c == '\n' as u8 {
                write_serial('\r' as u8);
            }
        }
        Ok(())
    }
}

#[doc(hidden)]
pub fn _print(args: core::fmt::Arguments) {
    use core::fmt::Write;

    Serial.write_fmt(args).unwrap();
}

/// Debug equivalent of the [`print`] macro. Only outputs to serial.
#[macro_export]
macro_rules! dbg_print {
    ($($arg:tt)*) => ($crate::serial::_print(format_args!($($arg)*)));
}

/// Debug equivalent of the [`println`] macro. Only outputs to serial.
#[macro_export]
macro_rules! dbg_println {
    () => ($crate::dbg_print!("\n"));
    ($($arg:tt)*) => ($crate::dbg_print!("{}\n", format_args!($($arg)*)));
}
