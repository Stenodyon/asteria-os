use crate::addr::ToAddr;

use super::{Addr, Phys};

/// Immutable reference in physical space.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct PhysRef<'a, T: ?Sized>(&'a T);

impl<'a, T> PhysRef<'a, T> {
    /// # Safety
    /// `ref`'s underlying pointer must be in physical space.
    pub unsafe fn from(r#ref: &'a T) -> Self {
        Self(r#ref)
    }
}

impl<T> PhysRef<'_, T> {
    /// Returns the physical address of this physical reference.
    pub fn address(&self) -> Addr<Phys> {
        let addr = self.0 as *const _ as u64;
        Addr::phys(addr)
    }
}

impl<'a, T> core::ops::Deref for PhysRef<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.0
    }
}

/// Mutable reference in physical space.
#[derive(Debug, PartialEq, Eq)]
#[repr(transparent)]
pub struct PhysRefMut<'a, T: ?Sized>(&'a mut T);

impl<'a, T> PhysRefMut<'a, T> {
    /// # Safety
    /// `ref`'s underlying pointer must be in physical space.
    pub unsafe fn from(r#ref: &'a mut T) -> Self {
        Self(r#ref)
    }
}

impl<'a, T> core::ops::Deref for PhysRefMut<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.0
    }
}

impl<'a, T> core::ops::DerefMut for PhysRefMut<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0
    }
}

/// Helper trait to transform values and references into [`PhysRef`].
pub trait AsPhysRef<T: ?Sized> {
    /// Transforms values and references into [`PhysRef`].
    fn as_phys_ref<'a>(&'a self) -> PhysRef<'a, T>;
}

/// Helper trait to transform mutable values and references into [`PhysRefMut`].
pub trait AsPhysRefMut<T: ?Sized> {
    /// Transforms mutable values and references into [`PhysRefMut`].
    fn as_phys_ref_mut(&mut self) -> PhysRefMut<T>;
}

impl<T: 'static> AsPhysRef<T> for T {
    fn as_phys_ref<'a>(&'a self) -> PhysRef<'a, T> {
        let phys_addr = self.to_addr().translate();
        let phys_ref = unsafe { phys_addr.to_ref() };
        PhysRef(phys_ref)
    }
}

impl<'a, T: 'static> AsPhysRef<T> for &'a T {
    fn as_phys_ref(&self) -> PhysRef<'a, T> {
        let phys_addr = self.to_addr().translate();
        let phys_ref = unsafe { phys_addr.to_ref() };
        PhysRef(phys_ref)
    }
}
