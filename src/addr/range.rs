use crate::x86_64::paging::{PageId, PageRange};

use super::{Addr, AddressSpaceMarker};

/// Inclusive address range.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AddressRange<T: AddressSpaceMarker> {
    start: Addr<T>,
    end: Addr<T>,
}

impl<T: AddressSpaceMarker> AddressRange<T> {
    /// Creates a new range from a given start and end address.
    ///
    /// # Panic
    /// Panics if end < start - 1.
    pub fn new(start: Addr<T>, end: Addr<T>) -> Self {
        assert!(end >= start.offset(-1));

        Self { start, end }
    }

    /// Creates a new range from a start address and a range length.
    pub const fn from_base_and_length(base: Addr<T>, length: usize) -> Self {
        Self {
            start: base,
            end: base.offset(length as i64 - 1),
        }
    }

    /// Returns the start address of the range.
    pub fn start(&self) -> Addr<T> {
        self.start
    }

    /// Returns the end address of the range.
    pub const fn end(&self) -> Addr<T> {
        self.end
    }

    /// Returns the length in bytes of the range.
    pub fn length(&self) -> usize {
        let length = self.end - self.start + 1;
        length as usize
    }

    /// Returns true if the address range contains the given address.
    pub fn contains(&self, addr: Addr<T>) -> bool {
        self.start <= addr && self.end >= addr
    }

    /// Returns the intersection of two ranges (if there is one, otherwise None).
    pub fn intersect(self, other: Self) -> Option<Self> {
        let start = self.start.max(other.start);
        let end = self.end.min(other.end);
        if start > end {
            return None;
        }
        Some(Self { start, end })
    }

    /// Returns true if the other range is completely included in this one.
    pub fn includes(self, other: Self) -> bool {
        match self.intersect(other) {
            Some(range) => range.length() == other.length(),
            None => false,
        }
    }

    /// Returns the page range covering this address range.
    pub fn page_range(self) -> PageRange<T> {
        let range = PageId::of_address(self.start)..=PageId::of_address(self.end);
        range.into()
    }

    /// Returns a slice over the bytes covered by this address range.
    pub unsafe fn bytes(self) -> &'static mut [u8] {
        core::slice::from_raw_parts_mut(self.start().as_ptr_mut(), self.length())
    }
}

impl<T: AddressSpaceMarker> core::fmt::Display for AddressRange<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}..{}", self.start, self.end)
    }
}
