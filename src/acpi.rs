use crate::deserialization::SizedStr;

/// RSDP version 2 structure as defined by the ACPI specification.
#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct RSDPv2 {
    signature: SizedStr<8>,
    checksum: u8,
    oem_id: SizedStr<6>,
    revision: u8,
    rsdt_address: u32,
    length: u32,
    xsdt_address: u64,
    extended_checksum: u8,
    reserved: [u8; 3],
}
