use core::mem::size_of;

use crate::addr::{Addr, AddressRange};
use crate::deserialization::Bytes;
use crate::geometry::{Rect, Vec2u};
use crate::ppm::PPM;
use crate::vspace::VSpaceAllocator;
use crate::x86_64::paging::AddressSpace;
use crate::x86_64::PrivilegeLevel;

/// Wrapper around the framebuffer provided by the multiboot protocol.
#[derive(Debug, Copy, Clone)]
pub struct Framebuffer {
    /// The framebuffer isn't necessarily in RAM, this address points to Memory-Mapped IO (i.e.
    /// writes don't go to RAM). This means that this address can be outside RAM and still be
    /// valid.
    base: *mut u8,
    pitch: usize,
    width: usize,
    height: usize,
    bpp: u8,
    color_info: FramebufferColorInfo,
}

impl Framebuffer {
    /// Returns the width of the framebuffer in pixels.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Returns the height of the framebuffer in pixels.
    pub fn height(&self) -> usize {
        self.height
    }

    /// Returns an origin-based rectangle whose width and height are the width and height in pixels
    /// of the framebuffer.
    pub fn bounds(&self) -> Rect {
        Rect::new(Vec2u::zero(), Vec2u::new(self.width, self.height))
    }

    /// Writes a pixel at the given x and y coordinates in pixels, with the given RGB value.
    pub fn put_pixel(&self, x: usize, y: usize, r: u8, g: u8, b: u8) {
        if self.bpp != 32 {
            todo!("bpp other than 32");
        }

        let FramebufferColorInfo::RGB {
            red_field_pos,
            red_mask_size,
            green_field_pos,
            green_mask_size,
            blue_field_pos,
            blue_mask_size,
        } = &self.color_info
        else {
            todo!("other framebuffer types than RGB");
        };

        if x >= self.width || y >= self.height {
            return;
        }

        let offset = (self.bpp as usize / 8) * x + self.pitch * y;

        let r = r as u32 & ((1 << red_mask_size) - 1);
        let g = g as u32 & ((1 << green_mask_size) - 1);
        let b = b as u32 & ((1 << blue_mask_size) - 1);
        let pixel = r << red_field_pos | b << blue_field_pos | g << green_field_pos;

        unsafe {
            *(self.base.offset(offset as isize) as *mut u32) = pixel;
        }
    }

    /// Blits an image onto the framebuffer.
    pub fn blit(&self, image: &PPM, dst: Vec2u, src_rect: Option<Rect>) {
        let Some(src_rect) = src_rect
            .unwrap_or_else(|| image.bounds())
            .intersection(image.bounds())
        else {
            return;
        };

        let Some(dst_rect) = Rect::new(dst, src_rect.size).intersection(self.bounds()) else {
            return;
        };

        for y in 0..src_rect.size.y {
            for x in 0..src_rect.size.x {
                let (r, g, b) = image.pixel_at(src_rect.base.x + x, src_rect.base.y + y);
                self.put_pixel(dst_rect.base.x + x, dst_rect.base.y + y, r, g, b);
            }
        }
    }

    /// Scrolls the entire framebuffer by `amount` pixels.
    pub fn scroll_up(&self, amount: usize) {
        for y in 0..self.height - amount {
            for x in 0..self.pitch / size_of::<u64>() {
                let src_offset = x * size_of::<u64>() + (y + amount) * self.pitch;
                let dst_offset = x * size_of::<u64>() + y * self.pitch;

                unsafe {
                    *(self.base.offset(dst_offset as isize) as *mut u64) =
                        *(self.base.offset(src_offset as isize) as *const u64);
                }
            }
        }

        // Clear the newly exposed part of the screen
        for y in self.height - amount..self.height {
            for x in 0..self.pitch / size_of::<u64>() {
                let offset = x * size_of::<u64>() + y * self.pitch;

                unsafe {
                    *(self.base.offset(offset as isize) as *mut u64) = 0;
                }
            }
        }
    }

    /// Moves the framebuffer to virtual space.
    ///
    /// # Safety
    /// This function should only be used *once*.
    pub unsafe fn move_to_virtual_space(mut self) -> Self {
        let base_address = Addr::phys(self.base as u64);
        let length = self.pitch * self.height;
        let range = AddressRange::from_base_and_length(base_address, length);
        let page_range = range.page_range();
        let virtual_space = VSpaceAllocator::alloc(page_range.count()).unwrap();
        let mut page_structure = AddressSpace::current();
        for (phys_page, virt_page) in page_range.zip(virtual_space) {
            page_structure
                .map_pages(virt_page, phys_page, PrivilegeLevel::Ring0)
                .unwrap();
        }

        self.base = virtual_space.start().base().as_ptr_mut();
        self
    }
}

/// Read the framebuffer from bytes, as specified by the multiboot protocol.
pub fn read_framebuffer_info(bytes: &mut Bytes) -> Result<Framebuffer, ()> {
    let address = bytes.read_ptr_mut().unwrap();
    let pitch = bytes.read_u32().unwrap() as usize;
    let width = bytes.read_u32().unwrap() as usize;
    let height = bytes.read_u32().unwrap() as usize;
    let bpp = bytes.read_u8().unwrap();
    let framebuffer_type = bytes.read_u8().unwrap();
    bytes.skip(1); // reserved byte

    // The specification doesn't mention it but it seems that the color_info field should be
    // aligned to 8 bytes.
    bytes.align_to(8);

    let color_info = match framebuffer_type {
        0 => {
            let num_colors = bytes.read_u32().ok_or(())? as usize;
            let ptr = bytes.ptr() as *const PaletteColor;
            let palette = unsafe { core::slice::from_raw_parts(ptr, num_colors) };

            FramebufferColorInfo::Indexed(palette)
        }

        1 => {
            let red_field_pos = bytes.read_u8().ok_or(())?;
            let red_mask_size = bytes.read_u8().ok_or(())?;
            let green_field_pos = bytes.read_u8().ok_or(())?;
            let green_mask_size = bytes.read_u8().ok_or(())?;
            let blue_field_pos = bytes.read_u8().ok_or(())?;
            let blue_mask_size = bytes.read_u8().ok_or(())?;

            FramebufferColorInfo::RGB {
                red_field_pos,
                red_mask_size,
                green_field_pos,
                green_mask_size,
                blue_field_pos,
                blue_mask_size,
            }
        }

        2 => FramebufferColorInfo::EGAText,
        _ => FramebufferColorInfo::Unkonwn(framebuffer_type),
    };

    let info = Framebuffer {
        base: address,
        pitch,
        width,
        height,
        bpp,
        color_info,
    };
    Ok(info)
}

/// Information for encoding color data when writing to the framebuffer.
#[derive(Debug, Clone, Copy)]
pub enum FramebufferColorInfo {
    /// The framebuffer uses an indexed palette.
    Indexed(&'static [PaletteColor]),

    /// The framebuffer supports RGB values in the given format.
    RGB {
        /// Position in bits of the `red` field.
        red_field_pos: u8,
        /// Length in bits of the `red` field.
        red_mask_size: u8,
        /// Position in bits of the `green` field.
        green_field_pos: u8,
        /// Length in bits of the `green` field.
        green_mask_size: u8,
        /// Position in bits of the `blue` field.
        blue_field_pos: u8,
        /// Length in bits of the `blue` field.
        blue_mask_size: u8,
    },

    /// The framebuffer supports EGA Text mode.
    EGAText,

    /// The framebuffer supports an encoding that is not specified (this is an error).
    Unkonwn(u8),
}

/// Color information for an indexed color framebuffer.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C, packed)]
pub struct PaletteColor {
    red: u8,
    green: u8,
    blue: u8,
}
