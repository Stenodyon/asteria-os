use crate::addr::{Addr, AddressRange, Phys};
use crate::deserialization::Bytes;

/// Memory map provided by the multiboot protocol.
#[derive(Clone, Copy)]
pub struct MemoryMap<'a> {
    entry_size: usize,
    data: &'a [u8],
}

impl<'a> MemoryMap<'a> {
    /// Reads a memory map from the given bytes.
    pub fn new(entry_size: usize, data: &'a [u8]) -> Self {
        Self { entry_size, data }
    }
}

impl Iterator for MemoryMap<'_> {
    type Item = Entry;

    fn next(&mut self) -> Option<Self::Item> {
        let entry_data = self.data.chunks(self.entry_size).next()?;
        let mut bytes = Bytes::new(entry_data);

        let base_address = Addr::<Phys>::from_value(bytes.read_u64().unwrap());
        let length = bytes.read_u64().unwrap() as usize;
        let entry_type = bytes.read_u32().unwrap().into();

        let address_range = AddressRange::from_base_and_length(base_address, length);

        let entry = Entry {
            address_range,
            entry_type,
        };

        self.data = &self.data[self.entry_size..];

        Some(entry)
    }
}

/// An entry in the memory map.
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Entry {
    address_range: AddressRange<Phys>,
    entry_type: EntryType,
}

impl Entry {
    /// Returns the type of the entry.
    pub fn entry_type(&self) -> EntryType {
        self.entry_type
    }

    /// Returns the physical address range of the entry.
    pub fn address_range(&self) -> AddressRange<Phys> {
        self.address_range
    }
}

impl core::fmt::Debug for Entry {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let start = self.address_range.start();
        let end = self.address_range.end();
        let length = self.address_range.length();

        write!(f, "{start}..{end} ({length:>16}) {}", self.entry_type)
    }
}

impl core::fmt::Debug for MemoryMap<'_> {
    fn fmt(&self, fmt: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        fmt.write_str("MemoryMap ")?;

        fmt.debug_set().entries(*self).finish()
    }
}

/// Type of the memory region entry.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EntryType {
    /// Memory available for use.
    Available = 1,
    /// Usable memory holding ACPI information.
    ACPI = 3,
    /// Reserved memory that must be preserved on hibernation.
    Reserved = 4,
    /// Defective RAM modules.
    Defective = 5,
    /// Unknown type.
    Unknown,
}

impl From<u32> for EntryType {
    fn from(value: u32) -> Self {
        match value {
            1 => EntryType::Available,
            3 => EntryType::ACPI,
            4 => EntryType::Reserved,
            5 => EntryType::Defective,
            _ => EntryType::Unknown,
        }
    }
}

impl core::fmt::Display for EntryType {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let name = match *self as u32 {
            1 => "Available RAM",
            3 => "ACPI",
            4 => "Reserved Memory",
            5 => "Defective RAM",
            _ => "Unknown (Reserved Area)",
        };
        write!(f, "{}", name)
    }
}
