//! Safe(r) abstractions around virtual and physical addressing.

/// Reference type wrappers for distinguishing between references in physical space and references
/// in virtual space.
mod refs;

/// Address ranges.
mod range;

use core::fmt::Debug;
use core::marker::PhantomData;
use core::mem::size_of;

pub use range::*;
pub use refs::*;

use crate::x86_64::paging::{get_level4_table, PageId, PAGE_SIZE};
use crate::KERNEL_BASE;

/// Denotes the physical address space.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Phys;

/// Denotes the virtual address space.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Virt;

/// Marker trait to group [`Phys`] and [`Virt`] together.
pub trait AddressSpaceMarker: Debug + Copy {
    /// Returns an instance of the type representing the address space.
    fn this() -> Self;
}

impl AddressSpaceMarker for Phys {
    fn this() -> Self {
        Self
    }
}

impl AddressSpaceMarker for Virt {
    fn this() -> Self {
        Self
    }
}

/// An address in a given address space.
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Addr<T: AddressSpaceMarker>(u64, PhantomData<T>);

// We can't use `#[repr(transparent)]` because there is currently no way to enforce that the `T`
// type parameter is zero-sized. So this is a sanity check just to make sure we're making no
// mistakes.
static_assert!(size_of::<Addr<Phys>>() == size_of::<u64>());
static_assert!(size_of::<Addr<Virt>>() == size_of::<u64>());

impl<T: AddressSpaceMarker> PartialEq for Addr<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T: AddressSpaceMarker> Eq for Addr<T> {}

impl<T: AddressSpaceMarker> PartialOrd for Addr<T> {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T: AddressSpaceMarker> Ord for Addr<T> {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T: AddressSpaceMarker> Addr<T> {
    /// Creates an address type from the given value.
    pub const fn from_value(value: u64) -> Self {
        Self(value, PhantomData)
    }

    /// Null (zero) address.
    pub const fn null() -> Self {
        Self(0, PhantomData)
    }

    /// Returns the address's value.
    pub const fn value(self) -> u64 {
        self.0
    }

    /// Returns the ID of the page the address is in.
    pub const fn page(self) -> PageId<T> {
        PageId::of_address(self)
    }

    /// Offsets the address by the given value.
    pub const fn offset(mut self, offset: i64) -> Self {
        if offset >= 0 {
            self.0 += offset as u64
        } else {
            self.0 -= (-offset) as u64
        }

        self
    }

    /// If the address is not page-aligned, aligns it to the next page.
    pub fn aligned_to_next_page(mut self) -> Self {
        self.0 = self.0.next_multiple_of(PAGE_SIZE as u64);
        self
    }

    /// Turns the address into a reference.
    pub unsafe fn to_ref<U>(self) -> &'static U {
        &*(self.0 as *const U)
    }

    /// Turns the address into a mutable reference.
    pub unsafe fn to_ref_mut<U>(self) -> &'static mut U {
        &mut *(self.0 as *mut U)
    }

    /// Creates a slice starting at the address, with the given amount of elements.
    pub unsafe fn to_slice<U>(self, count: usize) -> &'static [U] {
        core::slice::from_raw_parts(self.0 as *const U, count)
    }

    /// Creates a mutable slice starting at the address, with the given amount of elements.
    pub unsafe fn to_slice_mut<U>(self, count: usize) -> &'static mut [U] {
        core::slice::from_raw_parts_mut(self.0 as *mut U, count)
    }

    /// Interprets this address as a pointer of the specified type.
    pub fn as_ptr<U>(self) -> *const U {
        self.0 as *const U
    }

    /// Interprets this address as a mutable pointer of the specified type.
    pub fn as_ptr_mut<U>(self) -> *mut U {
        self.0 as *mut U
    }
}

impl Addr<Phys> {
    /// Creates a new address in physical space from the given value.
    pub const fn phys(value: u64) -> Addr<Phys> {
        Addr(value, PhantomData)
    }

    /// Translates this address into kernel (virtual) space.
    pub fn to_kernel_space(self) -> Addr<Virt> {
        Addr(self.0 + KERNEL_BASE, PhantomData)
    }
}

impl Addr<Virt> {
    /// Creates a new address from the given value.
    pub const fn virt(value: u64) -> Addr<Virt> {
        Addr(value, PhantomData)
    }

    /// Returns the index into the level 4 page table for this address.
    pub fn level_4_index(&self) -> usize {
        // masking bits 39 to 47 (9 bits)
        let mask = (1 << 9) - 1;
        (self.0 as usize >> 39) & mask
    }

    /// Returns the index into the level 3 page table for this address.
    pub fn level_3_index(&self) -> usize {
        // masking bits 30 to 38 (9 bits)
        let mask = (1 << 9) - 1;
        (self.0 as usize >> 30) & mask
    }

    /// Returns the index into the level 2 page table for this address.
    pub fn level_2_index(&self) -> usize {
        // masking bits 21 to 29 (9 bits)
        let mask = (1 << 9) - 1;
        (self.0 as usize >> 21) & mask
    }

    /// Returns the offset into the 2MiB page this address is in (for level-2 huge pages).
    pub fn level_2_offset(&self) -> u64 {
        // masking bits 0 to 20
        let mask = (1 << 21) - 1;
        self.0 & mask
    }

    /// Use the page table structures to translate this virtual address into its corresponding
    /// physical address.
    pub fn translate(&self) -> Addr<Phys> {
        let base = unsafe {
            let p4 = get_level4_table();

            let p3 = p4[self.level_4_index()]
                .get_pointed_to_table()
                .expect("Page not present");
            let entry = p3[self.level_3_index()];
            // FIXME: handle huge level 3 pages
            assert!(!entry.is_huge());

            let p2 = entry.get_pointed_to_table().expect("Page not present");
            let entry = p2[self.level_2_index()];
            // FIXME: handle non-huge level 2 pages
            assert!(entry.is_huge());

            entry.get_base_address()
        };

        base.offset(self.level_2_offset() as i64).into()
    }
}

impl<T: AddressSpaceMarker> Debug for Addr<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Addr<{:?}>({:#016x})", self.1, self.0)
    }
}

impl<T: AddressSpaceMarker> core::fmt::Display for Addr<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "0x{:016x}", self.0)
    }
}

impl<T: AddressSpaceMarker> core::ops::Add<i64> for Addr<T> {
    type Output = Addr<T>;

    fn add(self, rhs: i64) -> Self::Output {
        Self(self.0.wrapping_add_signed(rhs), PhantomData)
    }
}

impl<T: AddressSpaceMarker> core::ops::Add<usize> for Addr<T> {
    type Output = Addr<T>;

    fn add(mut self, rhs: usize) -> Self::Output {
        self.0 += rhs as u64;
        self
    }
}

impl<T: AddressSpaceMarker> core::ops::Sub for Addr<T> {
    type Output = i64;

    fn sub(self, rhs: Self) -> Self::Output {
        self.0 as i64 - rhs.0 as i64
    }
}

impl<T: AddressSpaceMarker> core::ops::Sub for &Addr<T> {
    type Output = i64;

    fn sub(self, rhs: Self) -> Self::Output {
        *self - *rhs
    }
}

impl<T: AddressSpaceMarker> core::ops::Sub<usize> for Addr<T> {
    type Output = Addr<T>;

    fn sub(mut self, rhs: usize) -> Self::Output {
        self.0 -= rhs as u64;
        self
    }
}

/// A trait implemented on safe pointer types to convert them to a [`Addr<Virt>`].
pub trait ToAddr {
    /// Helper to convert a reference into an address.
    fn to_addr(self) -> Addr<Virt>;
}

impl<T> ToAddr for &T {
    fn to_addr(self) -> Addr<Virt> {
        let addr = self as *const _ as u64;
        Addr::from_value(addr)
    }
}

impl<T> ToAddr for &mut T {
    fn to_addr(self) -> Addr<Virt> {
        let addr = self as *const _ as u64;
        Addr::from_value(addr)
    }
}
