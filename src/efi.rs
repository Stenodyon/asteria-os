use core::ffi::{c_char, c_void, CStr};

use crate::addr::{Addr, Phys};

/// Structures to read the memory map provided by EFI.
pub mod memory_map;

/// Header that is present at the beginning of all EFI tables.
#[derive(Debug)]
#[repr(C)]
pub struct TableHeader {
    signature: u64,
    revision: u32,
    header_size: u32,
    crc32: u32,
    reserved: u32,
}

/// Layout-compatible structure for the EFI System Table.
#[repr(C)]
pub struct SystemTable<'a> {
    header: TableHeader,
    firmware_vendor: *const c_char,
    firmware_revision: u32,

    console_in_handle: &'a c_void,
    // EFI_SIMPLE_TEXT_INPUT_PROTOCOL
    con_in: Addr<Phys>,

    console_out_handle: &'a c_void,
    // EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL
    con_out: Addr<Phys>,

    standard_error_handle: &'a c_void,
    // EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL
    std_err: Addr<Phys>,

    // TODO
    runtime_services: Addr<Phys>,
    boot_services: Addr<Phys>,

    table_entries_count: u64,
    configuration_table: &'a c_void,
}

impl core::fmt::Debug for SystemTable<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("SystemTable")
            .field("header", &self.header)
            .field("firmware_vendor", unsafe {
                &CStr::from_ptr(self.firmware_vendor)
            })
            .field("console_in_handle", &self.console_in_handle)
            .field("con_in", &self.con_in)
            .field("console_out_handle", &self.console_out_handle)
            .field("con_out", &self.con_out)
            .field("standard_error_handle", &self.standard_error_handle)
            .field("std_err", &self.std_err)
            .field("runtime_services", &self.runtime_services)
            .field("boot_services", &self.boot_services)
            .field("table_entries_count", &self.table_entries_count)
            .field("configuration_table", &self.configuration_table)
            .finish()
    }
}
