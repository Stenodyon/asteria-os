use crate::println;

pub struct Logger;

static LOGGER: Logger = Logger;

impl Logger {
    pub fn init() {
        log::set_logger(&LOGGER).unwrap();
    }
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        println!(
            "{}:{} -- {}",
            record.level(),
            record.target(),
            record.args()
        );
    }

    fn flush(&self) {}
}
