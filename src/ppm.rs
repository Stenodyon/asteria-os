//! PPM image format reader.

use core::num::ParseIntError;
use core::str::{from_utf8, Utf8Error};

use crate::geometry::{Rect, Vec2u};

/// An image in the PPM format.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PPM<'a> {
    width: usize,
    height: usize,
    data: &'a [u8],
}

impl<'a> PPM<'a> {
    /// Loads an image from bytes. The PPM format *must* be `P6`.
    pub fn from_bytes(bytes: &'a [u8]) -> Result<Self, Error<'a>> {
        let mut ppm = bytes.splitn(4, |&x| x == 0xa);

        let signature = ppm.next().ok_or(Error::UnexpectedEOF)?;
        if signature != [b'P', b'6'] {
            return Err(Error::InvalidSignature(signature));
        }

        let mut dimensions = from_utf8(ppm.next().ok_or(Error::UnexpectedEOF)?)?.split(' ');
        let width = dimensions
            .next()
            .ok_or(Error::UnexpectedEOF)?
            .parse::<usize>()?;
        let height = dimensions
            .next()
            .ok_or(Error::UnexpectedEOF)?
            .parse::<usize>()?;

        let max_value = from_utf8(ppm.next().ok_or(Error::UnexpectedEOF)?)?.parse::<u8>()?;

        if max_value != 255 {
            return Err(Error::UnimplementedMaxValue(max_value));
        }

        let data = ppm.next().ok_or(Error::UnexpectedEOF)?;

        if data.len() < 3 * width * height {
            return Err(Error::UnexpectedEOF);
        }

        let ppm = PPM {
            width,
            height,
            data,
        };

        Ok(ppm)
    }

    /// Image width in pixels.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Image height in pixels.
    pub fn height(&self) -> usize {
        self.height
    }

    /// Bounds of the image. A rectangle set at the origin whose size is the image's size.
    pub fn bounds(&self) -> Rect {
        Rect::new(Vec2u::zero(), Vec2u::new(self.width, self.height))
    }

    /// Returns the pixel (r, g, b) at the given (x, y) position. Panics if the position is out of
    /// bounds.
    pub fn pixel_at(&self, x: usize, y: usize) -> (u8, u8, u8) {
        if x >= self.width || y >= self.height {
            panic!(
                "position ({x}, {y}) out of bounds ({}, {})",
                self.width, self.height
            );
        }

        let offset = (x + y * self.width) * 3;

        (
            self.data[offset],
            self.data[offset + 1],
            self.data[offset + 2],
        )
    }
}

/// Errors that can come up when reading a PPM file.
#[derive(Debug, Clone)]
pub enum Error<'a> {
    /// The file's PPM signature was incorrect.
    InvalidSignature(&'a [u8]),
    /// The file data wasn't provided in its entirety.
    UnexpectedEOF,
    /// Error when converting the header into UTF-8.
    FromUtf8Error(Utf8Error),
    /// Error when parsing integers from the header.
    ParseIntError(ParseIntError),
    /// Encountered a maximum value that wasn't 255 (currently unsupported).
    UnimplementedMaxValue(u8),
}

impl core::fmt::Display for Error<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::InvalidSignature(signature) => write!(f, "invalid signature: \"{signature:?}\""),
            Self::UnexpectedEOF => write!(f, "unexpected end of file"),
            Self::FromUtf8Error(source) => write!(f, "utf8 error while parsing header: {source}"),
            Self::ParseIntError(source) => write!(f, "parse error while parsing header: {source}"),
            Self::UnimplementedMaxValue(value) => write!(f, "unsupported max value {value}"),
        }
    }
}

impl From<Utf8Error> for Error<'_> {
    fn from(value: Utf8Error) -> Self {
        Self::FromUtf8Error(value)
    }
}

impl From<ParseIntError> for Error<'_> {
    fn from(value: ParseIntError) -> Self {
        Self::ParseIntError(value)
    }
}
