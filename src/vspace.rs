use crate::addr::Virt;
use crate::spinlock::Mutex;
use crate::stack;
use crate::x86_64::paging::{PageId, PageRange};

/// First page from which virtual space is allocated, up to the kernel stack allocator's virtual
/// space.
pub const BASE_PAGE: PageId<Virt> = PageId::from_indices([511, 256, 0, 0]);

static V_SPACE_ALLOCATOR: Mutex<VSpaceAllocator> = Mutex::new(VSpaceAllocator::new());

/// Allocates contiguous virtual pages.
#[derive(Debug, Clone, Copy)]
pub struct VSpaceAllocator {
    current: usize,
}

impl VSpaceAllocator {
    const fn new() -> Self {
        Self { current: 0 }
    }

    /// Returns the number of pages available to the allocator.
    pub fn capacity() -> usize {
        let page_range = BASE_PAGE..stack::BASE_PAGE;
        page_range.count()
    }

    /// Allocates `amount` continguous virtual pages. `amount` must be non-zero.
    ///
    /// # Error
    /// Returns [`VAllocError::NotEnoughPages`] if there aren't enough pages left to fullfill the
    /// request.
    /// Returns [`VAllocError::InvalidArgument`] if the `amount` passed is zero.
    ///
    /// # Panic
    /// Panics if [`VSpaceAllocator::init`] hasn't been called.
    pub fn alloc(amount: usize) -> Result<PageRange<Virt>, VAllocError> {
        V_SPACE_ALLOCATOR.lock().alloc_impl(amount)
    }

    fn alloc_impl(&mut self, amount: usize) -> Result<PageRange<Virt>, VAllocError> {
        if amount == 0 {
            return Err(VAllocError::InvalidArgument);
        }

        if self.current + amount >= Self::capacity() {
            return Err(VAllocError::NotEnoughPages);
        }

        let current_page = BASE_PAGE + self.current;
        let end_page = current_page + amount - 1;
        let range = PageRange::from(current_page..=end_page);
        Ok(range)
    }
}

/// Returned when the page allocation failed.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum VAllocError {
    /// Returned when the virtual space allocator doesn't have enough pages left to fullfill the
    /// allocation request.
    NotEnoughPages,
    /// Returned when the amount of pages requested is zero.
    InvalidArgument,
}
