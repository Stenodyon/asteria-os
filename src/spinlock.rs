//! Mutex implementation using a very simple spinlock.

use core::cell::UnsafeCell;
use core::ops::{Deref, DerefMut};
use core::sync::atomic::{AtomicUsize, Ordering};

/// Mutex synchronization primitive.
pub struct Mutex<T> {
    lock: AtomicUsize,
    value: UnsafeCell<T>,
}

impl<T> Mutex<T> {
    /// Creates a new mutex wrapping the given value.
    pub const fn new(value: T) -> Self {
        Self {
            lock: AtomicUsize::new(0),
            value: UnsafeCell::new(value),
        }
    }

    /// Request to lock the mutex. Will block (and spin the CPU) until it obtains the lock.
    pub fn lock(&self) -> MutexGuard<T> {
        while self
            .lock
            .compare_exchange(0, 1, Ordering::Relaxed, Ordering::Relaxed)
            .is_err()
        {}

        MutexGuard { mutex: &self }
    }
}

unsafe impl<T> Sync for Mutex<T> {}

/// Represents ownership of a mutex's lock. Will unlock the mutex when dropped.
pub struct MutexGuard<'a, T> {
    mutex: &'a Mutex<T>,
}

impl<T> Drop for MutexGuard<'_, T> {
    fn drop(&mut self) {
        self.mutex.lock.store(0, Ordering::Relaxed);
    }
}

impl<T> Deref for MutexGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { &*self.mutex.value.get() }
    }
}

impl<T> DerefMut for MutexGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.mutex.value.get() }
    }
}
