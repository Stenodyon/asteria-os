use core::arch::asm;
use core::mem::MaybeUninit;

use crate::addr::{Addr, Virt};
use crate::println;
use crate::stack::{KernelStack, KernelStackAllocator, KernelStackPtr};
use crate::x86_64::gdt::GdtSelector;
use crate::x86_64::paging::AddressSpace;
use crate::x86_64::PrivilegeLevel;

use self::storage::{ThreadId, THREAD_STORAGE};

/// Global thread storage.
pub mod storage;

/// Pointer to the current thread's kernel stack.
#[no_mangle]
static mut KERNEL_STACK: MaybeUninit<KernelStackPtr> = MaybeUninit::uninit();

/// Thread absraction. Stores all the context needed to resume a thread.
#[derive(Debug)]
pub struct Thread {
    register_context: RegisterContext,
    address_space: AddressSpace,
    kernel_stack: KernelStack,

    rflags: u64,
    code_segment: u64,

    state: ThreadState,
}

impl Thread {
    /// Creates a new thread with the given address space.
    pub fn new(
        address_space: AddressSpace,
        entry_point: Addr<Virt>,
        privilege_level: PrivilegeLevel,
    ) -> &'static Self {
        let kernel_stack = KernelStackAllocator::alloc();
        let register_context = RegisterContext {
            rip: entry_point.value(),
            ..Default::default()
        };
        let code_segment = match privilege_level {
            PrivilegeLevel::Ring0 => GdtSelector::KernelCode,
            PrivilegeLevel::Ring3 => GdtSelector::UserCode,
        } as u64;

        let this = Self {
            register_context,
            address_space,
            kernel_stack,
            rflags: 0,
            code_segment,
            state: ThreadState::Running,
        };

        println!(
            "Thread storage contains {} free slots",
            THREAD_STORAGE.assume_init_ref().free_slots()
        );
        let id = unsafe { THREAD_STORAGE.assume_init_mut().add_thread(this).unwrap() };
        unsafe { THREAD_STORAGE.assume_init_ref().get(id).unwrap() }
    }

    /// Getter for the thread's kernel stack.
    pub fn kernel_stack(&mut self) -> &mut KernelStack {
        &mut self.kernel_stack
    }

    /// Resume execution of the thread.
    pub fn resume(&self) -> ! {
        assert!(self.state == ThreadState::Running);

        if self.address_space != AddressSpace::current() {
            unsafe { self.address_space.switch_to() };
        }
        unsafe { KERNEL_STACK.write(KernelStackPtr::from(&self.kernel_stack)) };

        let stack = [
            self.register_context.r15,
            self.register_context.r14,
            self.register_context.r13,
            self.register_context.r12,
            self.register_context.r11,
            self.register_context.r10,
            self.register_context.r9,
            self.register_context.r8,
            self.register_context.rbp,
            self.register_context.rsi,
            self.register_context.rdi,
            self.register_context.rdx,
            self.register_context.rcx,
            self.register_context.rbx,
            self.register_context.rax,
            // InterruptStackFrame
            self.register_context.rip,
            self.code_segment,
            self.rflags,
            self.register_context.rsp,
            0, // SS, unused
        ];

        unsafe {
            asm!(
            // construction an InterruptStackFrame on the stack.
            "mov rsp, {stack}",
            "pop r15",
            "pop r14",
            "pop r13",
            "pop r12",
            "pop r11",
            "pop r10",
            "pop r9",
            "pop r8",
            "pop rbp",
            "pop rsi",
            "pop rdi",
            "pop rdx",
            "pop rcx",
            "pop rbx",
            "pop rax",
            // Here the stack should countain the InterruptStackFrame
            "iretq",
            stack = in (reg) (&stack).as_ptr(),
            options(noreturn),
            )
        }
    }

    /// Kills the thread, stops it from executing any further.
    pub fn kill(&mut self) {
        println!("Killing thread {}", self.id());
        self.state = ThreadState::Killed;
    }

    /// Reads the Thread ID from the thread's kernel stack.
    pub fn id(&self) -> ThreadId {
        KernelStackPtr::from(&self.kernel_stack).thread_id()
    }

    /// Returns the currently executing thread.
    pub fn current() -> &'static mut Thread {
        let id = unsafe { KERNEL_STACK.assume_init_ref().thread_id() };
        unsafe { THREAD_STORAGE.assume_init_mut().get_mut(id).unwrap() }
    }
}

/// Represents the register context of a thread.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct RegisterContext {
    /// Saved RIP.
    pub rip: u64,

    /// Saved RAX.
    pub rax: u64,
    /// Saved RBX.
    pub rbx: u64,
    /// Saved RCX.
    pub rcx: u64,
    /// Saved RDX.
    pub rdx: u64,
    /// Saved RDI.
    pub rdi: u64,
    /// Saved RSI.
    pub rsi: u64,
    /// Saved RBP.
    pub rbp: u64,
    /// Saved RSP.
    pub rsp: u64,
    /// Saved R8.
    pub r8: u64,
    /// Saved R9.
    pub r9: u64,
    /// Saved R10.
    pub r10: u64,
    /// Saved R11.
    pub r11: u64,
    /// Saved R12.
    pub r12: u64,
    /// Saved R13.
    pub r13: u64,
    /// Saved R14.
    pub r14: u64,
    /// Saved R15.
    pub r15: u64,
}

/// The state a thread can be in.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ThreadState {
    /// The thread is currently running.
    Running,

    /// The thread is waiting on some operation to finish and will resume running when this
    /// operation is complete.
    Waiting,

    /// The thread has been killed and will be removed from storage.
    Killed,
}
