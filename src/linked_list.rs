use core::marker::PhantomData;

use crate::allocs::Allocator;

/// A linked list data structure.
pub struct LinkedList<T: 'static, Alloc: Allocator<Node<T>>> {
    root: Option<*mut Node<T>>,
    allocator: Alloc,
}

impl<T, Alloc: Allocator<Node<T>>> LinkedList<T, Alloc> {
    /// Constructs a new linked list with the given allocator.
    pub fn new(allocator: Alloc) -> Self {
        Self {
            root: None,
            allocator,
        }
    }

    /// Returns an immutable iterator over the elements of the list.
    pub fn iter<'a>(&'a self) -> Iter<'a, T> {
        Iter {
            current: self.root,
            phantom: PhantomData,
        }
    }

    /// Appends the value at the end of the list.
    pub fn push_back(&mut self, value: T) {
        let node = Node {
            next: None,
            previous: None,
            value,
        };
        let node = self.allocator.alloc(node).unwrap().leak();
        match self.last_node() {
            Some(last_node) => {
                unsafe { (*node).previous = Some(last_node as *mut Node<T>) };
                last_node.next = Some(node);
            }
            None => self.root = Some(node),
        }
    }

    /// Returns the first value for which the predicate `pred` returns true. Returns None if none
    /// of the values match.
    pub fn find(&mut self, mut pred: impl FnMut(&T) -> bool) -> Option<&T> {
        self.node_iter().map(|node| &node.value).find(|x| pred(x))
    }

    /// Removes the first element for which the predicate `pred` returns true.
    pub fn remove_by(&mut self, mut pred: impl FnMut(&T) -> bool) {
        let Some(to_remove) = self.node_iter().find(|node| pred(&node.value)) else {
            return;
        };

        if let Some(previous) = to_remove.previous {
            unsafe { (*previous).next = to_remove.next };
        }

        if let Some(next) = to_remove.next {
            unsafe { (*next).previous = to_remove.previous };
        }
    }

    fn last_node<'a>(&'a mut self) -> Option<&'a mut Node<T>> {
        let mut next = self.root?;
        loop {
            match unsafe { (*next).next } {
                Some(node) => next = node,
                None => return Some(unsafe { next.as_mut().unwrap() }),
            }
        }
    }

    fn node_iter(&mut self) -> NodeIter<T> {
        NodeIter {
            current: self.root,
            phantom: PhantomData,
        }
    }
}

impl<T, Alloc: Allocator<Node<T>>> Drop for LinkedList<T, Alloc> {
    fn drop(&mut self) {
        let mut next = self.root;
        while let Some(node) = next {
            next = unsafe { (*node).next };
            unsafe { node.drop_in_place() };
            self.allocator.free(node);
        }
    }
}

impl<T, Alloc> core::fmt::Debug for LinkedList<T, Alloc>
where
    T: core::fmt::Debug,
    Alloc: Allocator<Node<T>>,
{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

/// A node in the linked list.
#[derive(Debug)]
pub struct Node<T> {
    next: Option<*mut Node<T>>,
    previous: Option<*mut Node<T>>,
    value: T,
}

/// Immutable iterator over the elements of the list.
pub struct Iter<'a, T> {
    current: Option<*mut Node<T>>,
    phantom: PhantomData<&'a T>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        let node = self.current?;
        let value = unsafe { &(*node).value };
        self.current = unsafe { (*node).next };
        Some(value)
    }
}

struct NodeIter<'a, T> {
    current: Option<*mut Node<T>>,
    phantom: PhantomData<&'a T>,
}

impl<'a, T> Iterator for NodeIter<'a, T> {
    type Item = &'a mut Node<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let node = unsafe { self.current?.as_mut().unwrap() };
        self.current = node.next;
        Some(node)
    }
}
