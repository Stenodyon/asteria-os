use core::arch::asm;

/// Represents a Machine Specific Register.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum MSR {
    /// Extended Feature Enable Register
    EFER = 0xc000_0080,

    /// 32-bit system call target.
    STAR = 0xc000_0081,

    /// 64-bit system call target.
    LSTAR = 0xc000_0082,

    /// System call flag mask.
    SFMASK = 0xc000_0084,
}

/// Writes the given value to the specified Model-Specific Register.
#[inline(always)]
pub unsafe fn msr_write(msr: MSR, value: u64) {
    let lo = (value & ((1 << 32) - 1)) as u32;
    let hi = ((value >> 32) & ((1 << 32) - 1)) as u32;
    asm!(
        "wrmsr",
        in("ecx") msr as u64,
        in("eax") lo,
        in ("edx") hi,
    );
}

/// Reads the value of the specified Model-Specific Register.
pub unsafe fn msr_read(msr: MSR) -> u64 {
    let hi: u32;
    let lo: u32;

    asm!(
        "rdmsr",
        in("ecx") msr as u64,
        out("eax") lo,
        out("edx") hi,
    );

    (hi as u64) << 32 | lo as u64
}
