use super::PrivilegeLevel;

/// Wrapper around the rFLAGS register.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct Rflags(u64);

/// Carry flag bit
const CF_BIT: usize = 0;
/// Parity flag bit
const PF_BIT: usize = 2;
/// Auxiliary flag bit
const AF_BIT: usize = 4;
/// Zero flag bit
const ZF_BIT: usize = 6;
/// Sign flag bit
const SF_BIT: usize = 7;
/// Trap flag bit
const TF_BIT: usize = 8;
/// Interrupt flag bit
const IF_BIT: usize = 9;
/// Direction flag bit
const DF_BIT: usize = 10;
/// Overflow flag bit
const OF_BIT: usize = 11;
/// Nested Task bit
const NT_BIT: usize = 14;
/// Resume flag bit
const RF_BIT: usize = 16;
/// Virtual-8086 Mode bit
const VM_BIT: usize = 17;
/// Alignment Check bit
const AC_BIT: usize = 18;
/// Virtual Interrupt flag bit
const VIF_BIT: usize = 19;
/// Virtual Interrupt Pending bit
const VIP_BIT: usize = 20;
/// ID flag bit
const ID_BIT: usize = 21;

impl Rflags {
    /// Returns an accessor to the Carry (CF) flag.
    pub fn carry(&mut self) -> Flag {
        self.flag(CF_BIT)
    }

    /// Returns an accessor to the Parity (CF) flag.
    pub fn parity(&mut self) -> Flag {
        self.flag(PF_BIT)
    }

    /// Returns an accessor to the Auxiliary (AF) flag.
    pub fn auxiliary(&mut self) -> Flag {
        self.flag(AF_BIT)
    }

    /// Returns an accessor to the Zero (ZF) flag.
    pub fn zero(&mut self) -> Flag {
        self.flag(ZF_BIT)
    }

    /// Returns an accessor to the Sign (SF) flag.
    pub fn sign(&mut self) -> Flag {
        self.flag(SF_BIT)
    }

    /// Returns an accessor to the Trap (TF) flag.
    pub fn trap(&mut self) -> Flag {
        self.flag(TF_BIT)
    }

    /// Returns an accessor to the Interrupt (IF) flag.
    pub fn interrupt(&mut self) -> Flag {
        self.flag(IF_BIT)
    }

    /// Returns an accessor to the Direction (DF) flag.
    pub fn direction(&mut self) -> Flag {
        self.flag(DF_BIT)
    }

    /// Returns an accessor to the Overflow (OF) flag.
    pub fn overflow(&mut self) -> Flag {
        self.flag(OF_BIT)
    }

    // TODO: IOPL

    /// Returns the I/O Privilege Level field.
    pub fn iopl(&self) -> PrivilegeLevel {
        todo!()
    }

    /// Sets the I/O Privilege Level field to the given value.
    pub fn set_iopl(&mut self, _privilege_level: PrivilegeLevel) {
        todo!()
    }

    /// Returns an accessor to the Nested Task (NT) bit.
    pub fn nested_task(&mut self) -> Flag {
        self.flag(NT_BIT)
    }

    /// Returns an accessor to the Resume (RF) flag.
    pub fn resume(&mut self) -> Flag {
        self.flag(RF_BIT)
    }

    /// Returns an accessor to the Virtual-8086 Mode (VM) bit.
    pub fn virtual_8086_mode(&mut self) -> Flag {
        self.flag(VM_BIT)
    }

    /// Returns an accessor to the Alignment Check (AC) bit.
    pub fn alignment_check(&mut self) -> Flag {
        self.flag(AC_BIT)
    }

    /// Returns an accessor to the Virtual Interrupt (VIF) flag.
    pub fn virtual_interrupt(&mut self) -> Flag {
        self.flag(VIF_BIT)
    }

    /// Returns an accessor to the Virtual Interrupt Pending (VIP) bit.
    pub fn virtual_interrupt_pending(&mut self) -> Flag {
        self.flag(VIP_BIT)
    }

    /// Returns an accessor to the ID (ID) flag.
    pub fn id(&mut self) -> Flag {
        self.flag(ID_BIT)
    }

    fn flag(&mut self, bit: usize) -> Flag {
        Flag {
            register: self,
            bit,
        }
    }

    fn read_bit(&self, bit: usize) -> bool {
        (self.0 & (1 << bit)) != 0
    }

    fn set_bit(&mut self, bit: usize, value: bool) {
        let mask = 1 << bit;

        if value {
            self.0 |= mask;
        } else {
            self.0 &= !mask
        }
    }
}

/// Flag accessor on [`Rflag`].
#[derive(Debug, PartialEq, Eq)]
pub struct Flag<'a> {
    register: &'a mut Rflags,
    bit: usize,
}

impl<'a> Flag<'a> {
    /// Returns the state of the flag.
    pub fn get(&self) -> bool {
        self.register.read_bit(self.bit)
    }

    /// Sets the state of the flag.
    pub fn set(&mut self, value: bool) {
        self.register.set_bit(self.bit, value);
    }
}
