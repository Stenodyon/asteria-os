//! Provides data structures and helper functions to manage the page tables of the system.
//!
//! The kernel runs in the higher half of the memory, and it has physical memory linearly mapped.
//!
//! Currently there are 8 page tables that are setup as follow:
//!
//! ```text
//! 0 - P4
//!     P4[0]   -> LOW P3
//!     P4[511] -> HIGH P3
//! 1 - LOW  P3
//!     LOW  P3[0] -> LOW P2 1
//!     LOW  P3[1] -> LOW P2 2
//!     LOW  P3[2] -> LOW P2 3
//!     LOW  P3[3] -> LOW P2 4
//! 2 - LOW  P2 1
//! 3 - LOW  P2 2
//! 4 - LOW  P2 3
//! 5 - LOW  P2 4
//! 6 - HIGH P3
//!     HIGH P3[511] -> HIGH P2
//! 7 - HIGH P2
//! ```

/// Page allocator.
mod allocator;

/// Page Ids.
mod indices;

/// Page table entry structure.
mod entry;

/// Page table structure.
mod table;

pub use allocator::*;
pub use entry::*;
pub use indices::*;
pub use table::*;

use core::arch::asm;

use crate::addr::{Addr, AsPhysRef, Phys, PhysRef, PhysRefMut, Virt};
use crate::println;

use super::{cr3, extended_cpu_features, set_cr3, PrivilegeLevel};

/// Size of a x86_64 page in bytes.
pub const PAGE_SIZE: usize = 4096;
/// Size of a x86_64 level-2 huge page in bytes.
pub const HUGE_PAGE_SIZE: usize = PAGE_SIZE * 512;

/// An entire page table hierarchy.
#[derive(Debug)]
pub struct AddressSpace(PhysRefMut<'static, PageTable>);

impl AddressSpace {
    /// Returns a new page structure where all entries are empty except for the ones mapping the
    /// kernel to higher memory.
    pub fn new() -> Self {
        let mut root_table = PageFrameAllocator::alloc_page_table().unwrap();
        let kernel_entry = &mut root_table[511];
        kernel_entry.set_base_address(unsafe { PAGE_TABLES[6].phys_addr() });
        kernel_entry.set_write(true);
        kernel_entry.set_present(true);
        Self(root_table)
    }

    /// Returns the current PageStructure in use.
    pub fn current() -> Self {
        let ref_mut = unsafe { PhysRefMut::from(get_level4_table()) };
        Self(ref_mut)
    }

    /// Switches to this address space.
    pub unsafe fn switch_to(&self) {
        let mut cr3 = cr3();
        cr3.set_table_base_address(self.0.phys_addr());
        set_cr3(cr3)
    }

    /// Maps a virtual page to a physical page. Will allocate pages to store tables as necessary.
    pub fn map_pages(
        &mut self,
        from: PageId<Virt>,
        to: PageId<Phys>,
        privilege_level: PrivilegeLevel,
    ) -> Result<(), MappingError> {
        println!("Mapping {from} to {to}");

        let l1_entry = self.generate_l1_entry(from, privilege_level)?;
        l1_entry.set_base_address(to.base());
        l1_entry.set_write(true);
        l1_entry.set_present(true);
        l1_entry.set_privilege_level(privilege_level);

        // Invalidate any TLB entries to the virtual page.
        unsafe {
            asm!("invlpg [{x}]", x = in(reg) from.base().value());
        }

        Ok(())
    }

    /// Unmaps the given virtual page. The physical page this virtual page was mapped to is
    /// returned if there was one.
    pub fn unmap(&mut self, page: PageId<Virt>) -> Option<PageId<Phys>> {
        self.try_get_l1_entry_mut(page).and_then(|entry| {
            let phys_page = entry.is_present().then(|| entry.get_base_address().page());

            entry.set_present(false);

            // Invalidate any TLB entries to the virtual page.
            unsafe {
                asm!("invlpg [{x}]", x = in(reg) page.base().value());
            }

            phys_page
        })
    }

    /// Maps the given virtual page as a stack guard page.
    pub fn map_guard_page(&mut self, page: PageId<Virt>) -> Result<(), MappingError> {
        let l1_entry = self.generate_l1_entry(page, PrivilegeLevel::Ring0)?;
        l1_entry.set_present(false);
        l1_entry.set_available_bits_bottom(GUARD_PAGE_MARKER);

        // Invalidate any TLB entries to the virtual page.
        unsafe {
            asm!("invlpg [{x}]", x = in(reg) page.base().value());
        }

        Ok(())
    }

    /// Free the allocated pages.
    pub fn free(self) {
        todo!()
    }

    fn generate_l1_entry(
        &mut self,
        page: PageId<Virt>,
        privilege_level: PrivilegeLevel,
    ) -> Result<&mut PageTableEntry, MappingError> {
        let l4_entry = &mut self[page.level_4_index()];
        let mut l3_table = if !l4_entry.is_present() {
            let table = PageFrameAllocator::alloc_page_table()?;

            l4_entry.set_base_address(table.phys_addr());
            l4_entry.set_write(true);
            l4_entry.set_present(true);
            l4_entry.set_privilege_level(privilege_level);
            table
        } else {
            // unwrap: is_present() has been checked
            unsafe { l4_entry.get_pointed_to_table().unwrap() }
        };

        let l3_entry = &mut l3_table[page.level_3_index()];
        let mut l2_table = if !l3_entry.is_present() {
            let table = PageFrameAllocator::alloc_page_table()?;

            l3_entry.set_base_address(table.phys_addr());
            l3_entry.set_write(true);
            l3_entry.set_present(true);
            l3_entry.set_privilege_level(privilege_level);
            table
        } else {
            if l3_entry.is_huge() {
                todo!("1GiB pages");
            }
            // unwrap: is_present() has been checked
            unsafe { l3_entry.get_pointed_to_table().unwrap() }
        };

        let l2_entry = &mut l2_table[page.level_2_index()];
        let mut l1_table = if !l2_entry.is_present() {
            let table = PageFrameAllocator::alloc_page_table()?;

            l2_entry.set_base_address(table.phys_addr());
            l2_entry.set_write(true);
            l2_entry.set_present(true);
            l2_entry.set_privilege_level(privilege_level);
            table
        } else {
            if l2_entry.is_huge() {
                self.split_huge_l2_page(l2_entry);
            }
            // unwrap: is_present() has been checked
            unsafe { l2_entry.get_pointed_to_table().unwrap() }
        };

        let l1_entry = &mut l1_table[page.level_1_index()];

        // Hacky way to allow returning a reference to the entry, otherwise Rust prevents us from
        // doing it because l1_table is a "temporary value".
        use crate::addr::ToAddr;
        Ok(unsafe { l1_entry.to_addr().to_ref_mut() })
    }

    fn split_huge_l2_page(&mut self, entry: &mut PageTableEntry) {
        assert!(entry.is_present() && entry.is_huge());

        let page_base = entry.get_base_address();
        let entry_is_write = entry.is_write_enabled();

        let mut l1_table = PageFrameAllocator::alloc_page_table().unwrap();
        entry.set_base_address(l1_table.phys_addr());
        entry.set_page_size(false);

        for (index, entry) in l1_table.iter_mut().enumerate() {
            entry.set_base_address(page_base + index * PAGE_SIZE);
            entry.set_write(entry_is_write);
            entry.set_present(true);
        }
    }

    /// If the given virtual page is mapped with regular-sized pages, returns its level 1 entry in
    /// the page table structure.
    pub fn try_get_l1_entry_mut(&self, page: PageId<Virt>) -> Option<&mut PageTableEntry> {
        let l4_entry = &self[page.level_4_index()];
        if !l4_entry.is_present() {
            return None;
        }

        let l3_table = unsafe { l4_entry.get_pointed_to_table().unwrap() };
        let l3_entry = &l3_table[page.level_3_index()];
        if !l3_entry.is_present() {
            return None;
        }

        let l2_table = unsafe { l3_entry.get_pointed_to_table().unwrap() };
        let l2_entry = &l2_table[page.level_2_index()];
        if !l2_entry.is_present() {
            return None;
        }
        let l1_table = unsafe { l2_entry.get_pointed_to_table().unwrap() };

        let l1_entry = &l1_table[page.level_1_index()];
        // Hacky way to allow returning a reference to the entry, otherwise Rust prevents us from
        // doing it because l1_table is a "temporary value".
        use crate::addr::ToAddr;
        Some(unsafe { l1_entry.to_addr().to_ref_mut() })
    }

    /// If the given virtual page is mapped with regular-sized pages, returns its level 1 entry in
    /// the page table structure.
    pub fn try_get_l1_entry(&self, page: PageId<Virt>) -> Option<&PageTableEntry> {
        self.try_get_l1_entry_mut(page).map(|entry| &*entry)
    }
}

impl core::ops::Deref for AddressSpace {
    type Target = PageTable;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl core::ops::DerefMut for AddressSpace {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl PartialEq for AddressSpace {
    fn eq(&self, other: &Self) -> bool {
        self.0.phys_addr() == other.0.phys_addr()
    }
}

impl Eq for AddressSpace {}

/// Error during [`PageStructure::map_pages`].
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MappingError {
    /// The page frame allocator returned [`NoAvailablePage`].
    NoAvailablePage,
    /// The virtual page was already mapped to a physical page.
    AlreadyMapped,
}

impl From<NoAvailablePage> for MappingError {
    fn from(_: NoAvailablePage) -> Self {
        Self::NoAvailablePage
    }
}

/// Returns the root (level 4) table in the paging structure currently in use.
///
/// # Safety
/// If the `CR3` register has been tampered with, this function will return an invalid reference.
pub unsafe fn get_level4_table() -> &'static mut PageTable {
    super::cr3().table_base_address().to_ref_mut()
}

/// Empty page table for const initialization.
const NULL_PAGE_TABLE: PageTable = PageTable([PageTableEntry(0); 512]);
/// Number of statically allocated pages (see top of the module).
const PAGE_TABLE_COUNT: usize = 8;

/// Page tables for the kernel working address space.
static mut PAGE_TABLES: [PageTable; PAGE_TABLE_COUNT] = [NULL_PAGE_TABLE; PAGE_TABLE_COUNT];

/// Initialize the paging structure.
pub fn setup_kernel_address_space() {
    if extended_cpu_features().page1g() {
        println!("TODO: Use 1GiB pages when possible");
    }

    unsafe {
        let first_p4 = &mut PAGE_TABLES[0][0];
        first_p4.set_base_address(PAGE_TABLES[1].phys_addr());
        first_p4.set_write(true);
        first_p4.set_present(true);

        for p3_index in 0..=3 {
            let p3_entry = &mut PAGE_TABLES[1][p3_index];
            p3_entry.set_base_address(PAGE_TABLES[2 + p3_index].phys_addr());
            p3_entry.set_write(true);
            p3_entry.set_present(true);

            for (index, p2_entry) in PAGE_TABLES[2 + p3_index].0.iter_mut().enumerate() {
                let address = index * 0x200000 + p3_index * 0x40000000;
                p2_entry.set_base_address(Addr::from_value(address as u64));
                // Using 2MiB pages
                p2_entry.set_page_size(true);
                p2_entry.set_write(true);
                p2_entry.set_present(true);
            }
        }

        let last_p4 = &mut PAGE_TABLES[0][511];
        last_p4.set_base_address(PAGE_TABLES[6].phys_addr());
        last_p4.set_write(true);
        last_p4.set_present(true);

        let last_p3 = &mut PAGE_TABLES[6][511];
        last_p3.set_base_address(PAGE_TABLES[7].phys_addr());
        last_p3.set_write(true);
        last_p3.set_present(true);

        for (index, p2_entry) in PAGE_TABLES[7].0.iter_mut().enumerate() {
            let address = index * 0x200000;
            p2_entry.set_base_address(Addr::from_value(address as u64));
            // Using 2MiB pages
            p2_entry.set_page_size(true);
            p2_entry.set_write(true);
            p2_entry.set_present(true);
        }

        // Setup guard page
        PAGE_TABLES[7][30].set_present(false);

        set_paging_root(PAGE_TABLES[0].as_phys_ref());
    }
}

#[inline(always)]
unsafe fn set_paging_root(base: PhysRef<PageTable>) {
    unsafe { asm!("mov cr3, {root}", root = in(reg) base.address().value()) };
}
