//! Helpers for using the `CPUID` instruction.
use core::arch::asm;

/// Output values from the CPUID instruction.
#[derive(Debug, Clone, Copy)]
#[allow(dead_code)]
pub struct CpuidOutput {
    eax: u32,
    ebx: u32,
    ecx: u32,
    edx: u32,
}

/// Calls the CPUID instruction with the given argument. Returns the output values (some may be
/// garbage).
pub unsafe fn cpuid(argument: u32) -> CpuidOutput {
    let eax: u32;
    let ebx: u32;
    let ecx: u32;
    let edx: u32;

    unsafe {
        asm!(
           "cpuid",
           "mov esi, ebx",
            in("eax") argument,
            lateout("eax") eax,
            out("esi") ebx,
            out("ecx") ecx,
            out("edx") edx,
        );
    }

    CpuidOutput { eax, ebx, ecx, edx }
}

/// Holds the results of looking up the extended CPU features using the CPUID instruction.
#[derive(Debug, Clone, Copy)]
pub struct ExtendedCpuFeatures(CpuidOutput);

/// Returns the result of looking up the extended CPU features using the CPUID instruction.
pub fn extended_cpu_features() -> ExtendedCpuFeatures {
    ExtendedCpuFeatures(unsafe { cpuid(0x80000001) })
}

impl ExtendedCpuFeatures {
    /// Returns true if 1 GiB pages are available.
    pub fn page1g(&self) -> bool {
        (self.0.edx & (1 << 26)) != 0
    }
}
