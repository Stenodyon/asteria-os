use crate::x86_64::{msr_write, MSR};

use super::{gdt::GdtSelector, msr_read, PrivilegeLevel};

/// Sets the system call handler.
///
/// Safety: the handler must be properly implemented for the SYSCALL calling convention.
pub unsafe fn set_syscall_handler(handler: unsafe extern "C" fn()) {
    let star = (GdtSelector::UserCode as u64 | PrivilegeLevel::Ring3 as u64) << 48
        | (GdtSelector::KernelCode as u64) << 32;
    let lstar = handler as *const () as u64;

    msr_write(MSR::STAR, star);
    msr_write(MSR::LSTAR, lstar);
    msr_write(MSR::SFMASK, 0);
}

/// Enables syscalls by setting bit 0 of the EFER MSR to 1. Required before using the SYSCALL and
/// SYSRET instructions otherwise an Invalid Opcode Exception is triggered.
pub fn enable_syscalls() {
    let mut efer = read_efer();
    efer.set_syscall_extension(true);
    write_efer(efer);
}

fn read_efer() -> Efer {
    let value = unsafe { msr_read(MSR::EFER) };
    Efer(value)
}

fn write_efer(efer: Efer) {
    unsafe { msr_write(MSR::EFER, efer.0) };
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
struct Efer(u64);

impl Efer {
    fn set_bit(&mut self, bit: usize, value: bool) {
        let mask = 1 << bit;
        if value {
            self.0 |= mask;
        } else {
            self.0 &= !mask;
        }
    }

    pub fn set_syscall_extension(&mut self, value: bool) {
        self.set_bit(0, value);
    }
}
