//! Interrupt vector management.

use core::arch::asm;
use core::marker::PhantomData;
use core::mem::{size_of, MaybeUninit};

use crate::addr::{Addr, Virt};

use super::Rflags;

const INTERRUPT_GATE_TYPE: u8 = 0xe;
const TRAP_GATE_TYPE: u8 = 0xf;

/// Helper trait to get the address of the interrupt handlers.
pub trait HasOffset {
    /// Address of the handler.
    fn offset(self) -> u64;
}

impl HasOffset for extern "x86-interrupt" fn(InterruptStackFrame) {
    fn offset(self) -> u64 {
        self as *const () as u64
    }
}

impl HasOffset for extern "x86-interrupt" fn(InterruptStackFrame, u64) {
    fn offset(self) -> u64 {
        self as *const () as u64
    }
}

/// Grouping trait for the two kinds of handlers: with, and without error code.
pub trait Handler {
    /// The type of the handler (must be a function type).
    type HandlerType: HasOffset;

    /// Returns a function pointer for the default handler for this handler type.
    fn default_handler() -> Self::HandlerType;

    /// Gate type (trap or interrupt).
    fn gate_type() -> u8;
}

/// Token type for exception handlers that do not take an error code.
pub struct ExceptionHandler;
/// Token type for exception handlers that take an error code.
pub struct ExceptionHandlerWithError;
/// Token type for interrupt handlers that do not take an error code.
pub struct InterruptHandler;

impl ExceptionHandler {
    extern "x86-interrupt" fn default_handler_impl(_frame: InterruptStackFrame) {
        panic!("Exception");
    }
}

impl Handler for ExceptionHandler {
    type HandlerType = extern "x86-interrupt" fn(InterruptStackFrame);

    fn default_handler() -> Self::HandlerType {
        Self::default_handler_impl
    }

    fn gate_type() -> u8 {
        TRAP_GATE_TYPE
    }
}

impl ExceptionHandlerWithError {
    extern "x86-interrupt" fn default_handler_impl(_frame: InterruptStackFrame, _error_code: u64) {
        panic!("Exception with error code!");
    }
}

impl Handler for ExceptionHandlerWithError {
    type HandlerType = extern "x86-interrupt" fn(InterruptStackFrame, u64);

    fn default_handler() -> Self::HandlerType {
        Self::default_handler_impl
    }

    fn gate_type() -> u8 {
        TRAP_GATE_TYPE
    }
}

impl InterruptHandler {
    extern "x86-interrupt" fn default_handler_impl(_frame: InterruptStackFrame) {
        panic!("Interrupt!");
    }
}

impl Handler for InterruptHandler {
    type HandlerType = extern "x86-interrupt" fn(InterruptStackFrame);

    fn default_handler() -> Self::HandlerType {
        Self::default_handler_impl
    }

    fn gate_type() -> u8 {
        INTERRUPT_GATE_TYPE
    }
}

/// An entry into the IDT.
#[derive(Debug)]
#[repr(C)]
pub struct Entry<H: Handler> {
    offset_1: u16,
    selector: u16,
    ist: u8,
    type_attributes: u8,
    offset_2: u16,
    offset_3: u32,
    reserved: u32,
    phantom: PhantomData<H>,
}
static_assert!(core::mem::size_of::<Entry<ExceptionHandler>>() == 16);
static_assert!(core::mem::size_of::<Entry<ExceptionHandlerWithError>>() == 16);

impl<H: Handler> Clone for Entry<H> {
    fn clone(&self) -> Self {
        let Self {
            offset_1,
            selector,
            ist,
            type_attributes,
            offset_2,
            offset_3,
            reserved: _,
            phantom: _,
        } = self;

        Self {
            offset_1: *offset_1,
            selector: *selector,
            ist: *ist,
            type_attributes: *type_attributes,
            offset_2: *offset_2,
            offset_3: *offset_3,
            reserved: 0,
            phantom: PhantomData,
        }
    }
}

impl<H: Handler> Copy for Entry<H> {}

impl<H: Handler> Entry<H> {
    /// Points the entry to the given interrupt handler.
    pub fn set_handler(&mut self, handler: H::HandlerType) {
        let offset = handler.offset();
        self.offset_1 = (offset & 0xffff) as u16;
        self.offset_2 = ((offset >> 16) & 0xffff) as u16;
        self.offset_3 = (offset >> 32) as u32;
    }

    /// Sets the Interrupt Stack Table (0-3) for this entry.
    pub fn set_ist(&mut self, ist: u8) {
        self.ist = ist;
    }
}

impl<H: Handler> Default for Entry<H> {
    fn default() -> Self {
        let mut this = Self {
            offset_1: 0,
            // GDT entry 1 (Kernel Code)
            selector: 1 << 3,
            ist: 0,
            // Present + Trap Gate
            type_attributes: (1 << 7) | H::gate_type(),
            offset_2: 0,
            offset_3: 0,
            reserved: 0,
            phantom: PhantomData,
        };
        this.set_handler(H::default_handler());

        this
    }
}

/// The Interrupt Descriptor Table. Holds entries for each interrupt vector.
#[repr(C, align(4096))]
pub struct Idt {
    /// Occurs when dividing any number by 0 using the DIV and IDIV instructions, or when the
    /// division result is too large to be represented in the destination.
    ///
    /// The saved instruction pointer points to the instruction that caused the exception.
    pub division_error: Entry<ExceptionHandler>,

    /// When the debug-exception mechanism is enabled, this exception can occur under several
    /// circumstances:
    /// - Instruction execution.
    /// - Instruction single-stepping.
    /// - Data read or write.
    /// - I/O read or write.
    /// - Task switch.
    /// - Debug-register access.
    /// - Executing INT1.
    pub debug: Entry<ExceptionHandler>,

    /// Occurs as a result of system logic signaling a non-maskable interrupt to the processor.
    ///
    /// It is an interrupt.
    pub non_maskable_interrupt: Entry<InterruptHandler>,

    /// Occurs when an INT3 instruction is executed. The INT3 is normally used by debug software to
    /// set instruction breakpoints by replacing instruction-opcode bytes with the INT3 opcode.
    pub breakpoint: Entry<ExceptionHandler>,

    /// Occurs as a result of executing an INTO instruction while the overflow bit in rFLAGS is set
    /// to 1.
    pub overflow: Entry<ExceptionHandler>,

    /// Occurs as a result of executing the BOUND instruction. The BOUND instruction compares an
    /// array index (first operand) with the lower bounds and upper bounds of an array (second
    /// operand). If the array index is not within the array boundary, this exception occurs.
    pub bound_range_exceeded: Entry<ExceptionHandler>,

    /// Occurs when an attempt is made to execute an invalid or undefined opcode.
    pub invalid_opcode: Entry<ExceptionHandler>,

    /// Occurs in (I think) floating-point contexts? See the manual.
    pub device_not_available: Entry<ExceptionHandler>,

    /// Occurs when the handling of an exception causes another exception.
    pub double_fault: Entry<ExceptionHandlerWithError>,

    /// This interrupt vector is reserved. It is for a discontinued exception.
    pub coprocessor_segment_overrun: Entry<ExceptionHandler>,

    /// Occurs as a result of a control transfer through a gate descriptor that results in an
    /// invalid stack-segment reference using an SS selector in the TSS.
    pub invalid_tss: Entry<ExceptionHandlerWithError>,

    /// Occurs when an attempt is made to load a segment or gate with a clear present bit.
    /// TODO: I don't know if this can occur in long mode.
    pub segment_not_present: Entry<ExceptionHandlerWithError>,

    /// TODO: I don't know if this can occur in long mode.
    pub stack_segment_fault: Entry<ExceptionHandlerWithError>,

    /// This exception can occur in *many* circumstances that cannot be listed exhaustively, refer
    /// to the manual.
    pub general_protection_fault: Entry<ExceptionHandlerWithError>,

    /// Can occur during a memory acess in any of the following situations:
    /// - A page-translation-table entry or physical page involved in translating the memory access
    ///   is not present in physical memory.
    /// - An attempt is made by the processor to load the instruction TLB with a translation for a
    ///   non-executable page.
    /// - The memory access fails the paging-protection checks (user/supervisor, read/write, or
    ///   both).
    /// - A reserved bit in one of the page-translation-table entries is set to 1.
    /// - A data access to a user-mode address caused a protection key violation.
    ///
    /// The virtual address that caused the Page Fault is stored in the CR2 register.
    pub page_fault: Entry<ExceptionHandlerWithError>,

    reserved_1: Entry<ExceptionHandler>,

    /// See the manual.
    pub x87_floating_point_exception: Entry<ExceptionHandler>,

    /// Occurs when an unaligned-memory data reference is performed while alignment checking is
    /// enabled.
    pub alignment_check: Entry<ExceptionHandlerWithError>,

    /// Model-specific, processor implementations are not required to support the Machine Check
    /// exception.
    pub machine_check: Entry<ExceptionHandler>,

    /// Handles unmasked SSE floating-point exceptions. See the manual.
    pub simd_floating_point_exception: Entry<ExceptionHandler>,

    reserved_3: Entry<ExceptionHandler>,

    /// See the manual. Related to shadow stacks.
    pub control_protection_exception: Entry<ExceptionHandlerWithError>,

    reserved_2: Entry<ExceptionHandler>,

    /// May be injected by the hypervisor into a guest VM to notify the VM of pending events.
    pub hypervisor_injection_exception: Entry<ExceptionHandler>,

    /// Generated when certain events occur inside a secure guest VM.
    pub vmm_communication_exception: Entry<ExceptionHandlerWithError>,

    /// Generated by security-sensitive events under SVM.
    pub security_exception: Entry<ExceptionHandlerWithError>,

    reserved_4: Entry<ExceptionHandler>,

    /// User-defined interrupts.
    pub interrupts: [Entry<InterruptHandler>; 256 - 32],
}
static_assert!(core::mem::size_of::<Idt>() == 4096);

impl Idt {
    /// Creates a new empty IDT.
    pub fn new() -> Self {
        Self::default()
    }

    /// Loads the IDT for use by the CPU.
    pub fn load(self) {
        unsafe {
            IDT.write(self);
            IDTR.write(Idtr::new(IDT.assume_init_ref()));
            let idtr_address = IDTR.assume_init_ref() as *const _ as u64;
            asm!("lidt [{x}]", x = in(reg) idtr_address);
        }
    }
}

impl Default for Idt {
    fn default() -> Self {
        Self {
            division_error: Entry::default(),
            debug: Entry::default(),
            non_maskable_interrupt: Entry::default(),
            breakpoint: Entry::default(),
            overflow: Entry::default(),
            bound_range_exceeded: Entry::default(),
            invalid_opcode: Entry::default(),
            device_not_available: Entry::default(),
            double_fault: Entry::default(),
            coprocessor_segment_overrun: Entry::default(),
            invalid_tss: Entry::default(),
            segment_not_present: Entry::default(),
            stack_segment_fault: Entry::default(),
            general_protection_fault: Entry::default(),
            page_fault: Entry::default(),
            reserved_1: Entry::default(),
            x87_floating_point_exception: Entry::default(),
            alignment_check: Entry::default(),
            machine_check: Entry::default(),
            simd_floating_point_exception: Entry::default(),
            reserved_3: Entry::default(),
            control_protection_exception: Entry::default(),
            reserved_2: Entry::default(),
            hypervisor_injection_exception: Entry::default(),
            vmm_communication_exception: Entry::default(),
            security_exception: Entry::default(),
            reserved_4: Entry::default(),
            interrupts: [Entry::default(); 256 - 32],
        }
    }
}

#[repr(C, packed)]
struct Idtr {
    size: u16,
    offset: u64,
}
static_assert!(size_of::<Idtr>() == 10);

impl Idtr {
    fn new(idt: &Idt) -> Self {
        Self {
            // One less than the size of the IDT in *bytes*.
            size: size_of::<Idt>() as u16 - 1,
            offset: idt as *const _ as u64,
        }
    }
}

static mut IDT: MaybeUninit<Idt> = MaybeUninit::uninit();
static mut IDTR: MaybeUninit<Idtr> = MaybeUninit::uninit();

/// Represents a type of CPU exception.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Exception {
    // TODO
    /// General Protection Fault.
    GeneralProtectionFault = 0xd,

    /// Page Fault.
    PageFault = 0xe,
    // TODO
}

/// Holds the stack frame when an interrupt handler is called.
#[derive(Debug, Clone, Copy)]
#[repr(C, align(8))]
pub struct InterruptStackFrame {
    /// SS register (only used for compatibility mode).
    pub stack_segment: u64,
    /// The stack pointer when the interrupt was serviced.
    pub stack_pointer: u64,
    /// RFLAGS register when the interrupt was serviced.
    pub rflags: Rflags,
    /// CS register (in our case the selector for either kernel or user code) when the interrupt
    /// was serviced.
    pub code_segment: u64,
    /// The instruction pointer's value when the interrupt was serviced.
    pub instruction_pointer: Addr<Virt>,
}
static_assert!(size_of::<InterruptStackFrame>() == 40);
