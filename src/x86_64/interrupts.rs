//! Exceptions and Interrupt handlers.

use core::arch::asm;

use crate::addr::{Addr, Virt};
use crate::idt::InterruptStackFrame;
use crate::println;
use crate::thread::Thread;
use crate::x86_64;

use super::gdt::GdtSelector;
use super::idt::Idt;
use super::paging::HugePageId;

/// Sets up the IDT with the exception handlers.
pub fn set_interrupt_handlers() {
    let mut idt = Idt::new();
    idt.general_protection_fault
        .set_handler(general_protection_fault_handler);
    idt.general_protection_fault.set_ist(1);
    idt.page_fault.set_handler(page_fault_handler);
    idt.page_fault.set_ist(1);
    idt.invalid_opcode.set_handler(invalid_opcode_handler);

    idt.load();

    // Setup the Task Register to point to the TSS, which contains the addresses of the interrupt
    // stacks that the handlers may use.
    unsafe { x86_64::ltr(GdtSelector::TSS) }
}

extern "x86-interrupt" fn general_protection_fault_handler(
    _stack_frame: InterruptStackFrame,
    error_code: u64,
) {
    Thread::current().kill();
    panic!("General Protection Fault: {error_code:x}");
}

extern "x86-interrupt" fn page_fault_handler(stack_frame: InterruptStackFrame, error_code: u64) {
    const STACK_PAGE: HugePageId<Virt> = HugePageId::from_indices([511, 511, 31]);
    const GUARD_PAGE: HugePageId<Virt> = HugePageId::from_indices([511, 511, 30]);
    const STACK_TOP: u64 = STACK_PAGE.address_range().end().offset(8).value();

    let faulty_virt_address: u64;
    unsafe {
        asm!("mov {x}, cr2", x = out(reg) faulty_virt_address);
    }
    let faulty_virt_address = Addr::<Virt>::from_value(faulty_virt_address);

    if faulty_virt_address.page().is_guard_page() || GUARD_PAGE.contains(faulty_virt_address) {
        // Reset stack
        unsafe { asm!("mov rsp, {stack_top}", stack_top = in(reg) STACK_TOP) };

        // oopsie
        panic!("Stack Overflow!");
    }

    let rip = stack_frame.instruction_pointer;
    println!("PAGE FAULT");
    println!("Guard page range: {}", GUARD_PAGE.address_range());
    println!("Address in violation: {faulty_virt_address}");
    println!("Saved RIP: {}", rip);

    let present_message = if (error_code & 1) != 0 {
        "page-protection violation"
    } else {
        "non-present page"
    };

    let write_message = if (error_code & (1 << 1)) != 0 {
        "caused by a write"
    } else {
        "caused by a read"
    };

    let instruction_message = if (error_code & (1 << 4)) != 0 {
        ", caused by an instruction fetch"
    } else {
        ""
    };

    println!(
        "Error code: {error_code:04x}, {present_message}, {write_message}{instruction_message}"
    );

    panic!("Unrecoverable error");
}

extern "x86-interrupt" fn invalid_opcode_handler(stack_frame: InterruptStackFrame) {
    println!("Invalid Opcode Exception");
    println!("\tat address {}", stack_frame.instruction_pointer);
    println!(
        "\topcode = {}",
        *stack_frame.instruction_pointer.as_ptr::<u8>()
    );
    panic!();
}
