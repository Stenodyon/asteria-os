use crate::addr::{Addr, Phys, PhysRefMut};
use crate::x86_64::PrivilegeLevel;

use super::PageTable;

/// Represents an entry in a [`PageTable`].
#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct PageTableEntry(pub u64);

impl PageTableEntry {
    /// Returns an empty entry, most importantly its Present bit is set to 0.
    pub const fn empty() -> Self {
        Self(0)
    }

    #[inline(always)]
    fn set_bit(&mut self, bit: usize, value: bool) {
        let mask = 1 << bit;

        if value {
            self.0 |= mask
        } else {
            self.0 &= !mask
        };
    }

    #[inline(always)]
    fn get_bit(&self, bit: usize) -> bool {
        let mask = 1 << bit;
        (self.0 & mask) != 0
    }

    /// Sets the Present bit (0) to the given value.
    pub fn set_present(&mut self, present: bool) {
        self.set_bit(0, present);
    }

    /// Returns true if the present bit (0) is 1.
    pub fn is_present(&self) -> bool {
        self.get_bit(0)
    }

    /// Sets the Read/Write access bit (1) to the given value.
    pub fn set_write(&mut self, write: bool) {
        self.set_bit(1, write);
    }

    /// Returns true if the Read/Write access bit (1) is 1.
    pub fn is_write_enabled(&self) -> bool {
        self.get_bit(1)
    }

    /// Sets the User/Supervisor access bit (2) to the given value.
    /// 0 is supervisor only (CPL 0, 1, and 2).
    /// 1 is user (CPL 3, but also 0, 1, and 2).
    pub fn set_privilege_level(&mut self, privilege_level: PrivilegeLevel) {
        let bit_value = match privilege_level {
            PrivilegeLevel::Ring0 => false,
            PrivilegeLevel::Ring3 => true,
        };

        self.set_bit(2, bit_value)
    }

    /// Sets the Page Size bit (7) to the given value. A true value means a huge page.
    pub fn set_page_size(&mut self, page_size: bool) {
        self.set_bit(7, page_size);
    }

    /// Sets the Base Address to the given value.
    pub fn set_base_address(&mut self, address: Addr<Phys>) {
        // The base address is at most 52 bits, and the first 12 must be zero because pages and
        // page tables are 4KiB aligned.
        let mask = ((1 << 52) - 1) & !((1 << 12) - 1);

        self.0 = self.0 & !mask | address.value() & mask;
    }

    /// Returns the base address of this entry.
    pub fn get_base_address(&self) -> Addr<Phys> {
        let mask = ((1 << 52) - 1) & !((1 << 12) - 1);
        let address = self.0 & mask;
        Addr::from_value(address)
    }

    /// Returns true if the Page Size bit (7) is 1.
    #[inline(always)]
    pub fn is_huge(&self) -> bool {
        self.page_size().is_huge()
    }

    /// Returns the state of the Page Size bit (7).
    pub fn page_size(&self) -> PageSize {
        if self.get_bit(7) {
            PageSize::Huge
        } else {
            PageSize::Regular
        }
    }

    /// If this entry is present, returns a reference to a [`PageTable`] that it would be pointing
    /// to. This is unsafe as it is not guaranteed that the entry points to a page table e.g. if
    /// this entry is in the level-1 table.
    pub unsafe fn get_pointed_to_table(&self) -> Option<PhysRefMut<'static, PageTable>> {
        if !self.is_present() {
            return None;
        }

        let table = self.get_base_address().to_ref_mut();
        Some(PhysRefMut::from(table))
    }

    /// Sets the 3 bits (11:9) that are available for the OS to use.
    pub fn set_available_bits_bottom(&mut self, value: u8) {
        let mask = ((1 << 3) - 1) << 9;
        self.0 = (self.0 & !mask) | ((value as u64 & 0b111) << 9);
    }

    /// Returns the value of the 3 bits (11:9) that are available for the OS to use.
    pub fn get_available_bits_bottom(&self) -> u8 {
        (self.0 >> 9) as u8 & 0b111
    }
}

impl core::fmt::Debug for PageTableEntry {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        if !self.is_present() {
            return write!(f, "PTE(Absent)");
        }

        f.debug_tuple("PTE")
            .field_with(|f| write!(f, "{:#016x}", self.0))
            .field(&self.get_base_address())
            .field(&self.page_size())
            .finish()
    }
}

/// Page size of a page entry, corresponds to the Page Size bit.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum PageSize {
    /// Page is not huge.
    Regular,

    /// Page is huge (1GiB/2MiB).
    Huge,
}

impl PageSize {
    /// Returns true if the page size is Huge.
    #[inline(always)]
    pub fn is_huge(self) -> bool {
        self == Self::Huge
    }
}
