//! Page frame allocation.

use log::trace;

use crate::addr::{Addr, AddressRange, AddressSpaceMarker, Phys, PhysRefMut};
use crate::bitmap::Bitmap;
use crate::kernel_end;
use crate::multiboot::{EntryType, MultibootInfo};
use crate::spinlock::Mutex;

use super::{PageId, PageTable, PAGE_SIZE};

/// Inclusive page range.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PageRange<T: AddressSpaceMarker> {
    start: PageId<T>,
    end: PageId<T>,
}

impl<T: AddressSpaceMarker> PageRange<T> {
    /// Returns the first PageId in the range.
    pub fn start(&self) -> PageId<T> {
        self.start
    }

    /// Returns the last PageId in the range.
    pub fn end(&self) -> PageId<T> {
        self.end
    }

    /// Returns the inclusive address range over this range of pages.
    pub fn address_range(&self) -> AddressRange<T> {
        AddressRange::new(
            self.start.address_range().start(),
            self.end.address_range().end(),
        )
    }

    /// Returns a static mutable slice over the bytes covered by the pages.
    pub fn bytes_mut(&self) -> &'static mut [u8] {
        unsafe { self.address_range().bytes() }
    }
}

impl<T: AddressSpaceMarker> From<core::ops::RangeInclusive<PageId<T>>> for PageRange<T> {
    fn from(value: core::ops::RangeInclusive<PageId<T>>) -> Self {
        Self {
            start: *value.start(),
            end: *value.end(),
        }
    }
}

impl<T: AddressSpaceMarker> Iterator for PageRange<T> {
    type Item = PageId<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.start > self.end {
            return None;
        }

        let value = self.start;
        self.start += 1;
        Some(value)
    }
}

static PAGE_FRAME_ALLOCATOR: Mutex<Option<PageFrameAllocator>> = Mutex::new(None);

/// Manages allocation and freeing of physical page tables.
#[derive(Debug)]
pub struct PageFrameAllocator {
    bitmap: Bitmap,
}

impl PageFrameAllocator {
    /// Initializes the allocator using the given information about memory. Must be called before
    /// any allocation operations can be performed.
    pub fn init(multiboot_info: &MultibootInfo) {
        let mut allocator = PAGE_FRAME_ALLOCATOR.lock();
        if allocator.is_some() {
            panic!("PageFrameAllocator::init called twice");
        }
        let _ = allocator.insert(Self::new(multiboot_info));
    }

    fn new(multiboot_info: &MultibootInfo) -> Self {
        let memory_map = multiboot_info.memory_map().expect("No memory map :(");

        let memory_size = memory_map
            .filter(|entry| entry.entry_type() == EntryType::Available)
            .map(|entry| entry.address_range().end().value())
            .max()
            .unwrap() as usize
            + 1;

        let memory_range = AddressRange::from_base_and_length(Addr::null(), memory_size);

        // Number of whole pages that can fit in memory.
        let memory_page_count = memory_size.div_euclid(PAGE_SIZE);
        // Enough bytes to cover every page with a bit.
        let bitmap_byte_count = (memory_page_count + 8 - 1).div_euclid(8);
        // How many pages are required to hold the bitmap itself.
        let bitmap_page_count = (bitmap_byte_count + PAGE_SIZE - 1).div_euclid(PAGE_SIZE);

        // The bitmap is page-aligned and located right after the kernel.
        let bitmap_start = kernel_end().aligned_to_next_page().to_kernel_space();
        let bitmap_slice = unsafe { bitmap_start.to_slice_mut(bitmap_byte_count) };
        let mut bitmap = Bitmap::new(bitmap_slice);
        bitmap.fill(false);

        let mut this = Self { bitmap };

        // Add areas available in the memory map.
        for entry in memory_map {
            if entry.entry_type() != EntryType::Available {
                continue;
            }

            if entry.address_range().intersect(memory_range).is_none() {
                continue;
            }

            for page in entry.address_range().page_range() {
                // Check that the page fits whole inside the availalbe address range.
                if !entry.address_range().includes(page.address_range()) {
                    continue;
                }

                this.set_page_status(page, PageStatus::Available);
            }
        }

        // Remove the mutliboot info structure from available pages.
        let multiboot_range =
            AddressRange::from_base_and_length(multiboot_info.addr(), multiboot_info.size());
        this.mark_unavailable(multiboot_range.page_range());

        // Remove the kernel from available pages.
        let allocator_end = kernel_end()
            .aligned_to_next_page()
            .offset((bitmap_page_count * PAGE_SIZE) as i64 - 1);

        // Kernel starts at 1MiB
        let kernel_start = Addr::phys(1024 * 1024);
        let kernel_range = AddressRange::new(kernel_start, allocator_end);
        this.mark_unavailable(kernel_range.page_range());

        // Setting page 0 as unavailable to avoid lots of issues with the way Rust handles null
        // pointers.
        this.set_page_status(PageId::phys(0), PageStatus::Taken);

        this
    }

    fn reclaim_multiboot_area_impl(&mut self, multiboot_info: MultibootInfo) {
        let multiboot_range =
            AddressRange::from_base_and_length(multiboot_info.addr(), multiboot_info.size());
        for page in multiboot_range.page_range() {
            self.set_page_status(page, PageStatus::Available);
        }
    }

    /// Reclaims the pages that contain the multiboot info. This takes ownership of the multiboot
    /// info structure to prevent further use of it, as it is now invalid.
    pub fn reclaim_multiboot_area(multiboot_info: MultibootInfo) {
        PAGE_FRAME_ALLOCATOR
            .lock()
            .as_mut()
            .expect("PageFrameAllocator::init must be called before calling reclaim_multiboot_area")
            .reclaim_multiboot_area_impl(multiboot_info)
    }

    fn set_page_status(&mut self, page: PageId<Phys>, status: PageStatus) {
        let value = match status {
            PageStatus::Taken => false,
            PageStatus::Available => true,
        };

        self.bitmap.set(page.value(), value);
    }

    /// Mark the given range of pages as unavailable for allocation.
    pub fn mark_unavailable(&mut self, range: PageRange<Phys>) {
        for page in range {
            self.set_page_status(page, PageStatus::Taken);
        }
    }

    fn alloc_impl(&mut self) -> Result<PageId<Phys>, NoAvailablePage> {
        let page_index = self.bitmap.find_first().ok_or(NoAvailablePage)?;
        self.bitmap.set(page_index, false);

        trace!("Page Allocator: Allocating page {page_index}");

        Ok(PageId::phys(page_index))
    }

    /// Allocate a physical page.
    pub fn alloc() -> Result<PageId<Phys>, NoAvailablePage> {
        PAGE_FRAME_ALLOCATOR
            .lock()
            .as_mut()
            .expect("PageFrameAllocator::init must be called before calling alloc")
            .alloc_impl()
    }

    fn free_impl(&mut self, page: PageId<Phys>) {
        trace!("Page Allocator: Freeing page {page}");

        self.set_page_status(page, PageStatus::Available);
    }

    /// Free a physical page.
    pub fn free(&mut self, page: PageId<Phys>) {
        PAGE_FRAME_ALLOCATOR
            .lock()
            .as_mut()
            .expect("PageFrameAllocator::init must be called before calling free")
            .free_impl(page)
    }

    fn alloc_page_table_impl(&mut self) -> Result<PhysRefMut<'static, PageTable>, NoAvailablePage> {
        let frame_id = self.alloc_impl()?;
        let page_table = unsafe { frame_id.base().to_ref_mut::<PageTable>() };
        *page_table = PageTable::empty();
        let phys_ref = unsafe { PhysRefMut::from(page_table) };
        Ok(phys_ref)
    }

    /// Allocate and initialize a page containing a page table.
    pub fn alloc_page_table() -> Result<PhysRefMut<'static, PageTable>, NoAvailablePage> {
        PAGE_FRAME_ALLOCATOR
            .lock()
            .as_mut()
            .expect("PageFrameAllocator::init must be called before calling alloc_page_table")
            .alloc_page_table_impl()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum PageStatus {
    Taken,
    Available,
}

/// Returned when trying to allocate a page and there are no available physical pages left.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct NoAvailablePage;
