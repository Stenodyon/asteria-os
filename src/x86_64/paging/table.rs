//! The same struct is used by all levels of the paging structure.

use core::marker::PhantomData;

use crate::addr::{Addr, Phys, ToAddr};

use super::{PageId, PageTableEntry};

/// Represents a 512-entry page table in the paging system.
#[derive(Debug)]
#[repr(align(4096))]
pub struct PageTable(pub [PageTableEntry; 512]);
static_assert!(core::mem::size_of::<PageTable>() == 4096);

impl PageTable {
    /// An empty page table where all entries are blank (i.e. not present).
    pub const fn empty() -> Self {
        Self([PageTableEntry::empty(); 512])
    }

    /// Returns the physical address at the beginning of the table.
    pub fn phys_addr(&self) -> Addr<Phys> {
        self.to_addr().translate()
    }

    /// Returns the physical page ID occupied by this table.
    pub fn page_id(&self) -> PageId<Phys> {
        PageId::<Phys>::of_address(self.phys_addr())
    }

    /// Returns an iterator over mutable entries in the table.
    pub fn iter_mut(&mut self) -> IterMut {
        IterMut {
            table: (&mut self[0]) as *mut PageTableEntry,
            current: 0,
            phantom: PhantomData,
        }
    }
}

impl core::ops::Index<usize> for PageTable {
    type Output = PageTableEntry;
    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl core::ops::IndexMut<usize> for PageTable {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

/// An iterator over the entries in a page table.
pub struct IterMut<'a> {
    table: *mut PageTableEntry,
    current: usize,
    phantom: PhantomData<&'a PageTable>,
}

impl<'a> Iterator for IterMut<'a> {
    type Item = &'a mut PageTableEntry;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current >= 512 {
            return None;
        }

        let entry = unsafe { &mut *self.table.add(self.current) };
        self.current += 1;

        Some(entry)
    }
}
