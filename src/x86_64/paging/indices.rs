use core::marker::PhantomData;

use crate::addr::{Addr, AddressRange, AddressSpaceMarker, Phys, Virt};

use super::{AddressSpace, HUGE_PAGE_SIZE, PAGE_SIZE};

/// Page frame index.
///
/// Starting at 0 for the first 4K in memory, up to the last 4K.
#[derive(Debug, Clone, Copy)]
pub struct PageId<T: AddressSpaceMarker>(usize, PhantomData<T>);

/// Marker bits in the page table entry for a guard page.
pub const GUARD_PAGE_MARKER: u8 = 0b101;

impl<T: AddressSpaceMarker> PageId<T> {
    /// Returns the page frame index that contains the given address.
    pub const fn of_address(address: Addr<T>) -> Self {
        Self(address.value() as usize / PAGE_SIZE, PhantomData)
    }

    /// Creates a new virtual page id from the indices in each level of the page structure (from
    /// highest to lowest).
    pub const fn from_indices(indices: [usize; 4]) -> Self {
        let mut index =
            indices[0] << (9 * 3) | indices[1] << (9 * 2) | indices[2] << 9 | indices[3];
        // sign-extend to 16 bits after the level-4 index.
        if indices[0] & (1 << 8) != 0 {
            index |= ((1 << 16) - 1) << (9 * 4);
        }
        Self(index, PhantomData)
    }

    /// Returns an address range over the frame specified by this index.
    pub const fn address_range(&self) -> AddressRange<T> {
        AddressRange::from_base_and_length(self.base(), PAGE_SIZE)
    }

    /// Returns the base address of the page.
    pub const fn base(&self) -> Addr<T> {
        let address = self.0 * PAGE_SIZE;
        Addr::from_value(address as u64)
    }

    /// Returns true if the page referenced by this page frame id contains the given address.
    pub fn contains(&self, addr: Addr<T>) -> bool {
        self.address_range().contains(addr)
    }

    /// Returns the page index as a number.
    pub const fn value(&self) -> usize {
        self.0
    }

    /// Returns a mutable slice over the page.
    pub unsafe fn bytes_mut(&self) -> &'static mut [u8] {
        self.base().to_slice_mut(PAGE_SIZE)
    }
}

impl PageId<Phys> {
    /// Creates a new physical page index.
    pub const fn phys(index: usize) -> Self {
        Self(index, PhantomData)
    }
}

impl PageId<Virt> {
    /// Creates a new virtual page index.
    pub const fn virt(index: usize) -> Self {
        Self(index, PhantomData)
    }

    /// Returns the level 4 page table index for the given page.
    pub const fn level_4_index(&self) -> usize {
        self.0.div_euclid(512 * 512 * 512).rem_euclid(512)
    }

    /// Returns the level 3 page table index for the given page.
    pub const fn level_3_index(&self) -> usize {
        self.0.div_euclid(512 * 512).rem_euclid(512)
    }

    /// Returns the level 2 page table index for the given page.
    pub const fn level_2_index(&self) -> usize {
        self.0.div_euclid(512).rem_euclid(512)
    }

    /// Returns the level 1 page table index for the given page.
    pub const fn level_1_index(&self) -> usize {
        self.0.rem_euclid(512)
    }

    /// Returns true if this page is mapped as a guard page.
    pub fn is_guard_page(&self) -> bool {
        AddressSpace::current()
            .try_get_l1_entry(*self)
            .map(|entry| entry.get_available_bits_bottom() == GUARD_PAGE_MARKER)
            .unwrap_or(false)
    }
}

impl<T: AddressSpaceMarker> PartialEq for PageId<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T: AddressSpaceMarker> Eq for PageId<T> {}

impl<T: AddressSpaceMarker> PartialOrd for PageId<T> {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T: AddressSpaceMarker> Ord for PageId<T> {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T: AddressSpaceMarker> core::fmt::Display for PageId<T> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "{:?}Page[{}]({})",
            T::this(),
            self.0,
            self.address_range()
        )
    }
}

impl<T: AddressSpaceMarker> core::ops::Add<usize> for PageId<T> {
    type Output = PageId<T>;

    fn add(mut self, rhs: usize) -> Self::Output {
        self.0 += rhs;
        self
    }
}

impl<T: AddressSpaceMarker> core::ops::AddAssign<usize> for PageId<T> {
    fn add_assign(&mut self, rhs: usize) {
        self.0 += rhs;
    }
}

impl<T: AddressSpaceMarker> core::ops::Sub for PageId<T> {
    type Output = isize;

    fn sub(self, rhs: Self) -> Self::Output {
        self.0 as isize - rhs.0 as isize
    }
}

impl<T: AddressSpaceMarker> core::ops::Sub for &PageId<T> {
    type Output = isize;

    fn sub(self, rhs: Self) -> Self::Output {
        *self - *rhs
    }
}

impl<T: AddressSpaceMarker> core::ops::Sub<usize> for PageId<T> {
    type Output = Self;

    fn sub(mut self, rhs: usize) -> Self::Output {
        self.0 -= rhs;
        self
    }
}

impl<T: AddressSpaceMarker> core::iter::Step for PageId<T> {
    fn steps_between(start: &Self, end: &Self) -> Option<usize> {
        if end < start {
            return None;
        }

        Some((end - start) as usize)
    }

    fn forward_checked(start: Self, count: usize) -> Option<Self> {
        Some(start + count)
    }

    fn backward_checked(start: Self, count: usize) -> Option<Self> {
        Some(start - count)
    }
}

/// Same as [`PageId`] but for huge (2 MiB) pages.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct HugePageId<T: AddressSpaceMarker>(usize, PhantomData<T>);

impl<T: AddressSpaceMarker> HugePageId<T> {
    /// Creates a new virtual page id from the indices in each level of the page structure (from
    /// highest to lowest).
    pub const fn from_indices(indices: [usize; 3]) -> Self {
        let mut index = indices[0] << (9 * 2) | indices[1] << 9 | indices[2];
        // sign-extend to 16 bits after the level-4 index.
        if indices[0] & (1 << 8) != 0 {
            index |= ((1 << 16) - 1) << (9 * 3);
        }
        Self(index, PhantomData)
    }

    /// Returns the base address of the page.
    pub const fn base(&self) -> Addr<T> {
        let address = self.0 * HUGE_PAGE_SIZE;
        Addr::from_value(address as u64)
    }

    /// Returns an address range over the frame specified by this index.
    pub const fn address_range(&self) -> AddressRange<T> {
        AddressRange::from_base_and_length(self.base(), HUGE_PAGE_SIZE)
    }

    /// Returns true if the page referenced by this page frame id contains the given address.
    pub fn contains(&self, addr: Addr<T>) -> bool {
        self.address_range().contains(addr)
    }
}
