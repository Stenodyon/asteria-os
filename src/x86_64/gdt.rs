//! Management of the Global Descriptor Table.
use core::arch::asm;
use core::mem::{size_of, MaybeUninit};

use crate::addr::{Addr, ToAddr, Virt};

use super::PrivilegeLevel;

#[repr(C, packed)]
struct Gdt {
    null_entry: SegmentDescriptor,
    kernel_code_segment: SegmentDescriptor,
    user_code_segment: SegmentDescriptor,
    tss: TSSDescriptor,
}
static_assert!(core::mem::size_of::<Gdt>() == 40);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct SegmentDescriptor(u64);

impl SegmentDescriptor {
    pub const fn null() -> Self {
        SegmentDescriptor(0)
    }

    pub const fn new_code(priviledge: PrivilegeLevel) -> Self {
        let long_bit = 1 << 53;
        let present_bit = 1 << 47;
        let priviledge_bits = (priviledge as u64) << 13;
        // 0 = System (LDT, TSS), 1 = User (Code, Data)
        let descriptor_type_bit = 1 << 44;
        // 0 = Data, 1 = Code
        let code_bit = 1 << 43;

        SegmentDescriptor(long_bit | priviledge_bits | present_bit | descriptor_type_bit | code_bit)
    }
}

#[repr(C, packed)]
struct TSSDescriptor {
    limit_1: u16,
    base_1: u16,
    base_2: u8,
    access_byte: u8,
    limit_2_flags: u8,
    base_3: u8,
    base_4: u32,
    reserved: u32,
}

/// 64-bit Task State Segment.
#[repr(C, packed)]
pub struct TSS {
    reserved_1: u32,

    /// Stack pointers for rings 0 to 2 (in that order).
    pub rsp: [u64; 3],

    reserved_2: u64,

    /// Stack pointers available for interrupts (set per-interrupt in the IDT).
    pub ist: [Addr<Virt>; 7],

    reserved_3: u64,
    reserved_4: u16,

    /// Offset to the I/O permissions bitmap from the TSS base address.
    iopb: u16,
}
static_assert!(core::mem::size_of::<TSS>() == 104);

/// Task Switch Segment that contains information about interrupt stacks.
pub static mut IST: TSS = TSS {
    reserved_1: 0,
    reserved_2: 0,
    reserved_3: 0,
    reserved_4: 0,

    rsp: [0; 3],
    ist: [Addr::<Virt>::null(); 7],
    iopb: size_of::<TSS>() as u16,
};

/// Set of selectors into the GDT.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u32)]
pub enum GdtSelector {
    /// This selector should not be used as it points to the null first entry in the GDT.
    NULL = 0,
    /// This selector points to the descriptor for kernel code (CPL 0).
    KernelCode = 1 << 3,
    /// This selector points to the descriptor for user code (CPL 3).
    UserCode = 2 << 3,
    /// This selector points to the system descriptor for the TSS.
    TSS = 3 << 3,
}

fn make_tss_descriptor(ist: &'static TSS) -> TSSDescriptor {
    let base = ist as *const _ as u64;
    let limit = size_of::<TSS>();

    TSSDescriptor {
        reserved: 0,

        limit_1: (limit & 0xffff) as u16,
        limit_2_flags: ((limit >> 16) & 0xf) as u8,

        // present bit + 64-bit TSS (avaialable) type
        access_byte: (1 << 7) | 0x9,

        base_1: (base & 0xffff) as u16,
        base_2: ((base >> 16) & 0xff) as u8,
        base_3: ((base >> 24) & 0xff) as u8,
        base_4: (base >> 32) as u32,
    }
}

fn make_gdt(ist: &'static TSS) -> Gdt {
    Gdt {
        null_entry: SegmentDescriptor::null(),
        kernel_code_segment: SegmentDescriptor::new_code(PrivilegeLevel::Ring0),
        user_code_segment: SegmentDescriptor::new_code(PrivilegeLevel::Ring3),
        tss: make_tss_descriptor(ist),
    }
}

#[repr(C, packed)]
struct Gdtr {
    limit: u16,
    offset: Addr<Virt>,
}
static_assert!(core::mem::size_of::<Gdtr>() == 10);

impl Gdtr {
    pub fn new(gdt: &Gdt) -> Gdtr {
        Gdtr {
            // From the Intel Software Developer Manual, p. 3099
            // > As with segments, the limit value is added to the base address to get the address of the
            // > last valid byte. A limit value of 0 results in exactly one valid byte. Because segment
            // > descriptors are always 8 bytes long, the GDT limit should always be one less than an
            // > integral multiple of eight (that is, 8N – 1).
            limit: (size_of::<Gdt>() - 1) as u16,
            offset: gdt.to_addr(),
        }
    }
}

static mut GDT: MaybeUninit<Gdt> = MaybeUninit::uninit();
static mut GDTR: MaybeUninit<Gdtr> = MaybeUninit::uninit();

/// Sets the GDT up for the rest of the kernel runtime.
pub fn load_gdt() {
    unsafe {
        // Place the first interrupt stack pointer right under the stack guard
        //IST.ist[0] = 0x200000 * 30;

        GDT.write(make_gdt(&IST));
        GDTR.write(Gdtr::new(GDT.assume_init_ref()));
        let gdtr_address = &GDTR as *const _ as u64;
        asm!("lgdt [{x}]", x = in(reg) gdtr_address);
    }
}
