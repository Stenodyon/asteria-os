#!/bin/bash

mkdir /mnt/boot_build
mount boot.img /mnt/boot_build
rm -rf /mnt/boot_build/*
cp -rL ./efi/* /mnt/boot_build
umount /mnt/boot_build
rmdir /mnt/boot_build
