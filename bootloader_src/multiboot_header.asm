%define MULTIBOOT2_MAGIC 0xe85250d6
; architecture 0 (protected mode i386)
%define ARCHITECTURE 0

section .multiboot_header
header_start:
	dd MULTIBOOT2_MAGIC          ; magic number (multiboot 2)
	dd ARCHITECTURE              ; architecture 0 (protected mode i386)
	dd header_end - header_start ; header length
	; checksum
	dd (1 << 32) - (MULTIBOOT2_MAGIC + ARCHITECTURE + (header_end - header_start))

	; insert optional multiboot tags here

	; required end tag
	dw 0 ; type
	dw 0 ; flags
	dd 8 ; size
header_end:
