%define KERNEL_BASE			   0xffffffffc0000000
%define PAGE_TABLE_ENTRY_COUNT 512
%define PAGE_TABLE_ENTRY_SIZE  8
%define _2MiB				   0x200000

global long_mode_start

extern multiboot_info
extern kernel_entry

extern p4_table
extern p3_table_high
extern p2_table_high

section .multiboot.text
bits 64
long_mode_start:
	; clean up sectors
	mov ax, 0
	mov ss, ax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	; === Finish setting up paging ===
	; map last P4 entry to P3_high
	mov eax, p3_table_high
	or eax, 0b11 ; present + writable
	mov[p4_table + 511 * PAGE_TABLE_ENTRY_SIZE], eax

	; map last P3_high entry to P2_high
	mov eax, p2_table_high
	or eax, 0b11 ; present + writable
	mov[p3_table_high + 511 * PAGE_TABLE_ENTRY_SIZE], eax

	; map each P2_high entry to a huge 2MiB page *starting at phys address 0*
	mov ecx, 0 ; counter variable

.loop:
	mov rax, _2MiB
	mul ecx
	or rax, 0b10000011 ; present + writable + huge
	mov [p2_table_high + ecx * PAGE_TABLE_ENTRY_SIZE], rax

	inc ecx
	cmp ecx, PAGE_TABLE_ENTRY_COUNT
	jne .loop

	; stack guard page
	mov dword [p2_table_high + 30 * PAGE_TABLE_ENTRY_SIZE], 0

	; moving the stack
	mov rsp, KERNEL_BASE + _2MiB * 32

	mov rdi, [multiboot_info]
	mov rax, kernel_entry
	call rax

	hlt
