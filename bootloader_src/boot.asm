%define PAGE_TABLE_ENTRY_COUNT 512
%define PAGE_TABLE_ENTRY_SIZE  8
%define _2MiB				   0x200000

global start
global error
extern long_mode_start

section .multiboot.text
bits 32
start:
	; ebx contains the 32-bit physical address of the multiboot information
	; structure
	mov [multiboot_info], ebx
	mov dword [multiboot_info + 4], 0

	; setup stack
	mov esp, stack_top

	; === check multiboot ===
	cmp eax, 0x36d76289
	jne error

	; === check CPUID ===

	; if CPUID is supported, we should be able to flip the ID bit (bit 21) in
	; the FLAGS register.

	; read FLAGS into eax
	pushfd
	pop eax

	; saving FLAGS into ecx
	mov ecx, eax

	; flipping the bit
	xor eax, 1 << 21

	; write back into FLAGS
	push eax
	popfd

	; read FLAGS again
	pushfd
	pop eax

	; restore FLAGS
	push ecx
	popfd

	; if eax equals ecx, that means the bit wasn't flipped and thus CPUID is
	; not supported
	cmp eax, ecx
	je error

	; === check long mode ===
	; now we can use CPUID to check whether long mode can be used

	; test if extended processor info is available
	mov eax, 0x80000000 ; argument passed to CPUID through eax
	cpuid
	cmp eax, 0x80000001 ; eax must be at least this big, otherwise the CPU is too old
	jb error

	; use extended info to test if long mode is available
	mov eax, 0x80000001 ; extended processor info
	cpuid
	test edx, 1 << 29   ; test of the LM-bit is set in the D register
	jz error            ; if it's not set, there is no long mode

	; === setup page tables ===
	; map first P4 entry to P3_low
	mov eax, p3_table_low
	or eax, 0b11 ; present + writable
	mov [p4_table], eax

	; map first P3_low entry to P2_low
	mov eax, p2_table_low
	or eax, 0b11 ; present + writable
	mov [p3_table_low], eax

	; map each P2_low entry to a huge 2MiB page
	mov ecx, 0 ; counter variable

.loop:
	mov eax, _2MiB
	mul ecx
	or eax, 0b10000011 ; present + writable + huge
	mov [p2_table_low + ecx * PAGE_TABLE_ENTRY_SIZE], eax

	inc ecx
	cmp ecx, PAGE_TABLE_ENTRY_COUNT
	jne .loop

	; === enable paging ===
	; load P4 to CR3 register
	mov eax, p4_table
	mov cr3, eax

	; enable PAE-flag in CR4 (Physical Address Extension)
	mov eax, cr4
	or eax, 1 << 5
	mov cr4, eax

	; set the long mode bit in the EFER MSR (Model Specific Register)
	mov ecx, 0xC0000080
	rdmsr
	or eax, 1 << 8
	wrmsr

	; enable paging in the cr0 register
	mov eax, cr0
	or eax, 1 << 31
	mov cr0, eax

	; === load the 64-bit GDT ===
	lgdt [gdt64.pointer]

	; set the CS code selector to gdt64.code by performing a long jump
	jmp gdt64.code:long_mode_start

error:
	hlt

section .multiboot.bss

global p4_table
global p3_table_high
global p2_table_high

align 4096
p4_table:
	resb 4096
p3_table_low:
	resb 4096
p2_table_low:
	resb 4096
p3_table_high:
	resb 4096
p2_table_high:
	resb 4096

stack_bottom:
	resb 512
stack_top:

global multiboot_info

multiboot_info:
	resq 1

section .multiboot.rodata
gdt64:
	; ENTRY 0
	dq 0 ; first entry must be zero

	; offset to the code segment (EQU defines a constant)
.code: equ $ - gdt64

	; ENTRY 1 (code segment)
	; 43 : executable
	; 44 : descriptor type (set for code and data)
	; 47 : present
	; 53 : 64-bit mode
	dq (1 << 43) | (1 << 44) | (1 << 47) | (1 << 53)

	; pointer that is passed to the LGDT instruction
.pointer:
	dw $ - gdt64 - 1 ; length - 1
	dq gdt64         ; location of the gdt
